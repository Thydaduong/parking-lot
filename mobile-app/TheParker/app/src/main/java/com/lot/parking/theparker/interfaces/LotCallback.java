package com.lot.parking.theparker.interfaces;

public interface LotCallback {
    void onCallback(String value);
}
