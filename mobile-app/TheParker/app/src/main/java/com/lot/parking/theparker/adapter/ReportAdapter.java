package com.lot.parking.theparker.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lot.parking.theparker.R;
import com.lot.parking.theparker.model.Report;

import java.util.ArrayList;
import java.util.List;

public class ReportAdapter extends RecyclerView.Adapter<ReportAdapter.ReportViewHolder> {

    public List<Report> reportList = new ArrayList<>();

    public ReportAdapter(List<Report> reports) {
        this.reportList = reports;
    }

    public class ReportViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle, tvCheckedIn, tvCheckedOut;

        public ReportViewHolder(View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.id_tv_title);
            tvCheckedIn = itemView.findViewById(R.id.id_tv_checked_in);
            tvCheckedOut = itemView.findViewById(R.id.id_tv_checked_out);

        }
    }

    @NonNull
    @Override
    public ReportViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ReportViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.card_report, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ReportViewHolder holder, int position) {

//        Report report = reportList.get(position);
//        String checkedIn = "Checked In: " + report.getCheckedInTime();
//        String checkedOut = "Checked Out" + report.getCheckedOutTime();
//        String titleText = report.getPlateReportTitle();
//        holder.tvTitle.setText(titleText);
//        holder.tvCheckedIn.setText(checkedIn);
//        holder.tvCheckedOut.setText(checkedOut);
    }

    @Override
    public int getItemCount() {
        return reportList.size();
    }
}
