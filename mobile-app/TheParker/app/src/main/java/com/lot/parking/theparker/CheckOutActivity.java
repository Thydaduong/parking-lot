package com.lot.parking.theparker;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.lot.parking.theparker.model.Report;

public class CheckOutActivity extends AppCompatActivity implements View.OnClickListener {

    private Spinner provinceSelection;
    private String khName;
    private String enName;
    private String plateName;
    private TextView enNameView;
    private Toolbar toolbar;
    private Report plateData;

    private EditText etKeyCheckOut;
    private Button btVerifyKeyCheckout;

    private String verifyKey;
    private String userId;
    private String currentLot;
    private FirebaseUser mAuth;
    private DatabaseReference parkerInfoRef;

    private Button btnCheckOut, btnCancel;
    private TextView tvPlateName, tvPlateId, textNotFound;
    private ViewGroup viewGroup;
    private ViewGroup checkOutControlVg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_check_out);

        mAuth = FirebaseAuth.getInstance().getCurrentUser();
        parkerInfoRef = FirebaseDatabase.getInstance().getReference();
        if (mAuth != null) {
            userId = mAuth.getPhoneNumber();
            userId = userId.replace("+855", "0");
        }

        Log.d("firebase_user", "currentUser: " + userId);

        toolbar = findViewById(R.id.check_out_toolbar);
        toolbar.setTitle("Check Out");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        etKeyCheckOut = findViewById(R.id.id_key_check_out);
        btVerifyKeyCheckout = findViewById(R.id.search_key_check_out);
        btVerifyKeyCheckout.setOnClickListener(this);

        btnCancel = findViewById(R.id.cancel_check_out);
        btnCancel.setOnClickListener(this);
        btnCheckOut = findViewById(R.id.approve_check_out);

        tvPlateName = findViewById(R.id.display_plate_name);
        tvPlateId = findViewById(R.id.display_plate_id);
        viewGroup = findViewById(R.id.check_out_found);
        viewGroup.setVisibility(View.GONE);
        textNotFound = findViewById(R.id.text_not_found);
        textNotFound.setVisibility(View.INVISIBLE);
        checkOutControlVg = findViewById(R.id.vg_control_check_out);
        checkOutControlVg.setVisibility(View.VISIBLE);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search_key_check_out:
                handleCheckOut();
                break;
            case R.id.cancel_check_out:
                viewGroup.setVisibility(View.GONE);
                textNotFound.setVisibility(View.INVISIBLE);
                break;
        }
    }


    private void handleCheckOut() {
        verifyKey = etKeyCheckOut.getText().toString().toUpperCase();

        if (verifyKey.length() == 6) {
            parkerInfoRef.child("parkerInformation").child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    currentLot = String.valueOf(dataSnapshot.child("workingAt").getValue());
                    checkOut(currentLot);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        } else {
            etKeyCheckOut.setError("Invalid key");
        }

    }

    private void checkOut(String lot) {
        final DatabaseReference reference = parkerInfoRef.child("parkingDetails").child(lot);
        reference.addValueEventListener(new ValueEventListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (final DataSnapshot mDataSnapshot : dataSnapshot.getChildren()) {
                    if (mDataSnapshot.child("verifyKey").getValue(String.class).contains(verifyKey)) {
                        if (String.valueOf(mDataSnapshot.child("timestampCheckedOut").getValue()).equals("")) {
                            viewGroup.setVisibility(View.VISIBLE);
                            String plateName= String.valueOf(mDataSnapshot.child("plateName").getValue());
                            String plateId = String.valueOf(mDataSnapshot.child("plateID").getValue());
                            final String mId = String.valueOf(mDataSnapshot.getKey());
                            tvPlateId.setText(plateId);
                            tvPlateName.setText(plateName);
                            textNotFound.setVisibility(View.INVISIBLE);
                            checkOutControlVg.setVisibility(View.INVISIBLE);
                            btnCheckOut.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    reference.child(mId).child("timestampCheckedOut").setValue(ServerValue.TIMESTAMP);
                                    reference.child(mId).child("checkedOutBy").setValue(userId);
                                    viewGroup.setVisibility(View.GONE);
                                    checkOutControlVg.setVisibility(View.VISIBLE);
                                    textNotFound.setVisibility(View.INVISIBLE);
                                }
                            });
                        } else {
                            textNotFound.setVisibility(View.VISIBLE);
                            textNotFound.setText("Already Checked");

                        }
                    } else {
                        textNotFound.setVisibility(View.VISIBLE);
                        textNotFound.setText("Didn't checked in");
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


}
