package com.lot.parking.theparker.model;

public class Report {

    private String checkedInBy, checkedOutBy;
    private String parkingLot;
    private String plateNameE, plateID, vehicleType;
    private int ticketPrice;
    private int ticketPriceOvernight;

    public Report() {
    }

    public Report(String checkedInBy, String checkedOutBy, String parkinglot, String plateNameE, String plateID, int ticketPrice, String vehicleType) {
        this.checkedInBy = checkedInBy;
        this.checkedOutBy = checkedOutBy;
        this.parkingLot = parkinglot;
        this.plateNameE = plateNameE;
        this.plateID = plateID;
        this.ticketPrice = ticketPrice;
        this.vehicleType = vehicleType;
    }

    public Report(String checkedInBy, String checkedOutBy, String parkingLot, String plateNameE, String plateID, String vehicleType, int ticketPrice, int ticketPriceOvernight) {
        this.checkedInBy = checkedInBy;
        this.checkedOutBy = checkedOutBy;
        this.parkingLot = parkingLot;
        this.plateNameE = plateNameE;
        this.plateID = plateID;
        this.vehicleType = vehicleType;
        this.ticketPrice = ticketPrice;
        this.ticketPriceOvernight = ticketPriceOvernight;
    }

    public String getCheckedInBy() {
        return checkedInBy;
    }

    public String getCheckedOutBy() {
        return checkedOutBy;
    }

    public String getParkingLot() {
        return parkingLot;
    }

    public String getPlateNameE() {
        return plateNameE;
    }

    public String getPlateID() {
        return plateID;
    }

    public int getTicketPrice() {
        return ticketPrice;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public int getTicketPriceOvernight() {
        return ticketPriceOvernight;
    }
}
