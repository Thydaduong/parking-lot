package com.lot.parking.theparker.model;

import com.google.gson.annotations.SerializedName;

public class JsonData {
    @SerializedName("kh")
    String khName;
    @SerializedName("en")
    String enName;
    int selectedCount = 0;

    public JsonData(String khName, String enName, int itemSelectedCount) {
        this.khName = khName;
        this.enName = enName;
        this.selectedCount = itemSelectedCount;
    }


    public String getKhName() {
        return khName;
    }

    public String getEnName() {
        return enName;
    }

    public int getItemSelectedCount() {
        return selectedCount;
    }
}
