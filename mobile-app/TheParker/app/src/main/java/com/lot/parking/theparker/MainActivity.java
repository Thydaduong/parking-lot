package com.lot.parking.theparker;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.lot.parking.theparker.model.JsonData;
import com.lot.parking.theparker.model.Report;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText editTextPlateNumber;
    private boolean doubleBackToExitPressedOnce = false;

    private boolean isCompleted = false;

    private String khName = "";
    private String enName = "";

    private ArrayList<String> mProvinceNames = new ArrayList<>();
    private Report plateData, checkedPlateInfo;

    private SimpleDateFormat dateFormat;
    private Calendar calendar;

    private FirebaseUser mAuth;
    private DatabaseReference databaseReference;

    private Spinner provinceSelection;
    private Button uploadDataButton, viewReportButton, printReportButton, newRecordButton;
    private ViewGroup reportControlView, plateView;
    private TextView enNameView;

    private String plateName, plateId;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance().getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        enNameView = findViewById(R.id.id_plate_en);

        editTextPlateNumber = findViewById(R.id.id_plate_code);

        reportControlView = findViewById(R.id.control_button_vg);
        plateView = findViewById(R.id.plate_view);
        uploadDataButton = findViewById(R.id.id_push_plate);
        uploadDataButton.setOnClickListener(this);


        printReportButton = findViewById(R.id.print_report_btn);
        printReportButton.setOnClickListener(this);

        newRecordButton = findViewById(R.id.new_record_btn);
        newRecordButton.setOnClickListener(this);

        provinceSelection = findViewById(R.id.spinner_province_selection);
        String json = ReadFromfile("province_set.json", getApplicationContext());
        Log.d("jsonaaa", "onCreate: " + json);
        JsonData[] provinces = new Gson().fromJson(json, JsonData[].class);

        final String[] khItems = new String[provinces.length];
        final String[] enItems = new String[provinces.length];
        final List<JsonData> provinceList = new ArrayList<>();
        for (int i = 0; i < khItems.length; i++) {
            khItems[i] = provinces[i].getKhName();
            enItems[i] = provinces[i].getEnName();
            String kh = provinces[i].getKhName();
            String en = provinces[i].getEnName();
            int count = provinces[i].getItemSelectedCount();
            provinceList.add(new JsonData(kh, en, count));
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, khItems);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        provinceSelection.setAdapter(dataAdapter);
        provinceSelection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                khName = provinceList.get(position).getKhName();
                enName = provinceList.get(position).getEnName();
                plateName = khName+"-"+ enName;
                enNameView.setText(enName);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.check_out:
                Toast.makeText(getApplicationContext(), "scan plate", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), AddParkingReportActivity.class
                ));
                return true;
            case R.id.sign_out_btn:
                signOut();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                Toast.makeText(getApplicationContext(), "clicked", Toast.LENGTH_SHORT).show();
                return true;
        }
    }

    private void signOut() {
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        finish();
    }

    private void startReportActivity() {
        startActivity(new Intent(MainActivity.this, DashboardActivity.class));
        Toast.makeText(getApplicationContext(), "View Report", Toast.LENGTH_SHORT).show();
    }


    private void newParkingRecord() {
        plateData = new Report();
        handleControlReportView(false);
    }

    private void uploadPlateData() {
        handlePlateData();

        handleControlReportView(true);
    }

    private String name, plateNum;
    private String checkedInBy, checkedOutBy, serialId;
    private String parkingLotId;
    private String ticketPrice;

    private void handlePlateData() {




        databaseReference.child("parkerInformation").child(mAuth.getPhoneNumber()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                checkedInBy = mAuth.getPhoneNumber() + String.valueOf(dataSnapshot.child("parkerName").getValue());
                parkingLotId = String.valueOf(dataSnapshot.child("workingAt").getValue());
                Log.d("logxxx", "onDataChange: " +checkedInBy + "lotId:"+ parkingLotId);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });





        name = plateName;
        plateNum = String.valueOf(editTextPlateNumber.getText());



    }

    private void handleControlReportView(boolean isCompleted) {
        if (isCompleted) {
            uploadDataButton.setVisibility(View.GONE);
            reportControlView.setVisibility(View.VISIBLE);
            provinceSelection.setEnabled(false);
            editTextPlateNumber.setEnabled(false);
        } else {
            provinceSelection.setEnabled(true);
            editTextPlateNumber.setEnabled(true);
            uploadDataButton.setVisibility(View.VISIBLE);
            reportControlView.setVisibility(View.GONE);
            editTextPlateNumber.setText("");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_push_plate:
                uploadPlateData();
                break;
            case R.id.new_record_btn:
                newParkingRecord();
                break;
            case R.id.print_report_btn:
                Toast.makeText(getApplicationContext(), "clicked", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    public String ReadFromfile(String fileName, Context context) {
        StringBuilder returnString = new StringBuilder();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;
        try {
            fIn = context.getResources().getAssets()
                    .open(fileName, Context.MODE_WORLD_READABLE);
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);
            String line = "";
            while ((line = input.readLine()) != null) {
                returnString.append(line);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (isr != null)
                    isr.close();
                if (fIn != null)
                    fIn.close();
                if (input != null)
                    input.close();
            } catch (Exception e2) {
                e2.getMessage();
            }
        }
        return returnString.toString();
    }


}
