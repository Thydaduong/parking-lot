package com.lot.parking.theparker.fragments;


import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.lot.parking.theparker.AddParkingReportActivity;
import com.lot.parking.theparker.R;
import com.lot.parking.theparker.model.JsonData;
import com.lot.parking.theparker.model.Report;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.UUID;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddMotorbikeReportFragment extends Fragment implements View.OnClickListener {

    private String khName, enName, plateName;
    private Report plateData;
    private Spinner provinceSelection;
    private FirebaseUser mAuth;
    private DatabaseReference databaseReference;

    private TextView enNameView;
    private EditText editTextPlateNumber;
    private Button uploadDataButton, viewReportButton, rePrintReportButton, newRecordButton;
    private ViewGroup reportControlView, plateView;


    protected static final String TAG = "TAG";
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    Button mScan, mPrint, mDisc;
    BluetoothAdapter mBluetoothAdapter;
    private UUID applicationUUID = UUID
            .fromString("00001101-0000-1000-8000-00805F9B34FB");
    private ProgressDialog mBluetoothConnectProgressDialog;
    private BluetoothSocket mBluetoothSocket;
    BluetoothDevice mBluetoothDevice;
    private int MAX_LENGTH =6;

    public AddMotorbikeReportFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_motorbike_report, container, false);


        mAuth = FirebaseAuth.getInstance().getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();

        editTextPlateNumber = view.findViewById(R.id.id_plate_code);

        reportControlView = view.findViewById(R.id.control_button_vg);
        plateView = view.findViewById(R.id.plate_view);
        uploadDataButton = view.findViewById(R.id.id_push_plate);
        uploadDataButton.setOnClickListener(this);

        rePrintReportButton = view.findViewById(R.id.print_report_btn);
        rePrintReportButton.setOnClickListener(this);

        newRecordButton = view.findViewById(R.id.new_record_btn);
        newRecordButton.setOnClickListener(this);
        provinceSelection = view.findViewById(R.id.spinner_province_selection);
        enNameView = view.findViewById(R.id.id_plate_en);

//        searchBluetoothDevices();
        initSpinner(view);

        return view;
    }




        String getVerifyKey(int len) {
            String DATA = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            Random RANDOM = new Random();
            StringBuilder sb = new StringBuilder(len);

            for (int i = 0; i < len; i++) {
                sb.append(DATA.charAt(RANDOM.nextInt(DATA.length())));
            }

            return sb.toString();
        }


    private void initSpinner(View view) {

        String json = ReadFromfile("province_set.json", getContext());
        Log.d("jsonaaa", "onCreate: " + json);
        JsonData[] provinces = new Gson().fromJson(json, JsonData[].class);

        final String[] khItems = new String[provinces.length];
        final String[] enItems = new String[provinces.length];
        final List<JsonData> provinceList = new ArrayList<>();
        for (int i = 0; i < khItems.length; i++) {
            khItems[i] = provinces[i].getKhName();
            enItems[i] = provinces[i].getEnName();
            String kh = provinces[i].getKhName();
            String en = provinces[i].getEnName();
            int count = provinces[i].getItemSelectedCount();
            provinceList.add(new JsonData(kh, en, count));
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, khItems);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        provinceSelection.setAdapter(dataAdapter);
        provinceSelection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                khName = provinceList.get(position).getKhName();
                enName = provinceList.get(position).getEnName();
                plateName = enName;
                enNameView.setText(enName);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_push_plate:
                if (((AddParkingReportActivity) getActivity()).isBluetoothConnected()){
                    onSubmitPlateData();
                } else {
                    ((AddParkingReportActivity) getActivity()).checkBluetoothDeviceConnection();
                }
                break;
            case R.id.new_record_btn:
                newParkingRecord();
                break;
            case R.id.print_report_btn:
                printTicket(parkingMotorReport, parkingLotName, parkingLotAddress, getCurrentDate(), verifyKey);
                Toast.makeText(getContext(), "reprinted", Toast.LENGTH_SHORT).show();
                break;
        }
    }


    private void newParkingRecord() {
        plateData = new Report();
        handleControlReportView(false);
    }

    private void onSubmitPlateData() {
        String checkText = String.valueOf(editTextPlateNumber.getText());
        if (checkText.length() > 8 || checkText.length() < 7) {
            editTextPlateNumber.setError("check this again");
        } else {
            handlePlateData();
            handleControlReportView(true);
        }
    }

    private String name, plateNum;
    private String checkedInBy, checkedOutBy;
    private String parkingLotId;
    private String parkingLotName;
    private String parkingLotAddress;
    private int ticketPrice;
    private int ticketPriceOvernight;
    private String vehicleType = "Motorbike";

    private Report parkingMotorReport;
    private Report printMotorTicket;
    String verifyKey="";
    private void handlePlateData() {

        String phoneNumber = mAuth.getPhoneNumber();
        phoneNumber = phoneNumber.replace("+855", "0");
        checkedOutBy = "";
        final String finalPhoneNumber = phoneNumber;
        databaseReference.child("parkerInformation").child(phoneNumber).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                checkedInBy = finalPhoneNumber + String.valueOf(dataSnapshot.child("parkerName").getValue());
                parkingLotId = String.valueOf(dataSnapshot.child("workingAt").getValue());
                getPriceTicket(parkingLotId);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        Log.d("parkinglotid", "handlePlateData: " + parkingLotId);

        name = plateName;
        plateNum = String.valueOf(editTextPlateNumber.getText()).toUpperCase();

        Log.d("plateNumber", "handlePlateData: " + plateNum);
    }

    private String getCurrentDate(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        String date = sdf.format(c.getTime());
        return date;
    }

    private void getPriceTicket(final String parkingLotId) {
        databaseReference.child("parkingLots").child(parkingLotId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ticketPrice = Integer.parseInt(String.valueOf(dataSnapshot.child("motoDefaultPrice").getValue()));
                ticketPriceOvernight = Integer.parseInt(String.valueOf(dataSnapshot.child("motoOvernightPrice").getValue()));
                parkingLotName = String.valueOf(dataSnapshot.child("parkingLotName").getValue());
                parkingLotAddress = String.valueOf(dataSnapshot.child("parkingLotAddress").getValue());
                parkingMotorReport = new Report(checkedInBy, checkedOutBy, parkingLotId, name, plateNum, vehicleType, ticketPrice, ticketPriceOvernight);
                uploadPlateData(parkingMotorReport);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void uploadPlateData(Report parkingMotorReport) {
        verifyKey = getVerifyKey(MAX_LENGTH);
        DatabaseReference parkingDetailReference = databaseReference.child("parkingDetails").child(parkingMotorReport.getParkingLot()).push();
        parkingDetailReference.child("checkedInBy").setValue(parkingMotorReport.getCheckedInBy());
        parkingDetailReference.child("checkedOutBy").setValue(parkingMotorReport.getCheckedOutBy());
        parkingDetailReference.child("plateID").setValue(parkingMotorReport.getPlateID());
        parkingDetailReference.child("plateName").setValue(parkingMotorReport.getPlateNameE());
        parkingDetailReference.child("vehicleType").setValue(parkingMotorReport.getVehicleType());
        parkingDetailReference.child("ticketPrice").setValue(parkingMotorReport.getTicketPrice());
        parkingDetailReference.child("ticketPriceOvernight").setValue(parkingMotorReport.getTicketPriceOvernight());
        parkingDetailReference.child("timestampCheckedIn").setValue(ServerValue.TIMESTAMP);
        parkingDetailReference.child("timestampCheckedOut").setValue("");
        parkingDetailReference.child("verifyKey").setValue(verifyKey);

        printMotorTicket = parkingMotorReport;
        printTicket(parkingMotorReport, parkingLotName, parkingLotAddress, getCurrentDate(), verifyKey);

    }


    private void printTicket(Report parkingMotorReport, String parkingLotName, String parkingLotAddress, String currentDate, String verifyKey){
        ((AddParkingReportActivity) getActivity()).printTicket(parkingMotorReport, parkingLotName, parkingLotAddress, currentDate, verifyKey);
    }



    private void handleControlReportView(boolean isCompleted) {
        if (isCompleted) {
            uploadDataButton.setVisibility(View.GONE);
            reportControlView.setVisibility(View.VISIBLE);
            provinceSelection.setEnabled(false);
            editTextPlateNumber.setEnabled(false);
        } else {
            provinceSelection.setEnabled(true);
            editTextPlateNumber.setEnabled(true);
            uploadDataButton.setVisibility(View.VISIBLE);
            reportControlView.setVisibility(View.GONE);
            editTextPlateNumber.setText("");
        }
    }

    public String ReadFromfile(String fileName, Context context) {
        StringBuilder returnString = new StringBuilder();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;
        try {
            fIn = context.getResources().getAssets()
                    .open(fileName, Context.MODE_WORLD_READABLE);
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);
            String line = "";
            while ((line = input.readLine()) != null) {
                returnString.append(line);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (isr != null)
                    isr.close();
                if (fIn != null)
                    fIn.close();
                if (input != null)
                    input.close();
            } catch (Exception e2) {
                e2.getMessage();
            }
        }
        return returnString.toString();
    }

}
