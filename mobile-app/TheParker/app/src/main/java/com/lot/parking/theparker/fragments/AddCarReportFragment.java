package com.lot.parking.theparker.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.lot.parking.theparker.R;
import com.lot.parking.theparker.model.JsonData;
import com.lot.parking.theparker.model.Report;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class AddCarReportFragment extends Fragment implements View.OnClickListener {
    private String khName, enName, plateName;
    private Spinner provinceSelection;
    private TextView enNameView;
    private EditText editTextPlateNumber;
    private Button uploadDataButton, viewReportButton, printReportButton, newRecordButton;
    private ViewGroup reportControlView, plateView;

    private FirebaseUser mAuth;
    private DatabaseReference databaseReference;


    private Report carPlateData;

    public AddCarReportFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_car_report, container, false);

        mAuth = FirebaseAuth.getInstance().getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();

        editTextPlateNumber = view.findViewById(R.id.id_plate_code_car);

        reportControlView = view.findViewById(R.id.car_control_button_vg);
        plateView = view.findViewById(R.id.plate_view);
        uploadDataButton = view.findViewById(R.id.check_in_plate_car);
        uploadDataButton.setOnClickListener(this);

        viewReportButton = view.findViewById(R.id.view_report_btn_car);
        viewReportButton.setOnClickListener(this);

        printReportButton = view.findViewById(R.id.print_report_btn_car);
        printReportButton.setOnClickListener(this);

        newRecordButton = view.findViewById(R.id.new_record_btn_car);
        newRecordButton.setOnClickListener(this);
        provinceSelection = view.findViewById(R.id.spinner_province_selection_car);
        enNameView = view.findViewById(R.id.id_plate_en_car);


        initSpinner(view);

        return view;
    }

    private void initSpinner(View view) {

        String json = ReadFromfile("province_set.json", getContext());
        Log.d("jsonaaa", "onCreate: " + json);
        JsonData[] provinces = new Gson().fromJson(json, JsonData[].class);

        final String[] khItems = new String[provinces.length];
        final String[] enItems = new String[provinces.length];
        final List<JsonData> provinceList = new ArrayList<>();
        for (int i = 0; i < khItems.length; i++) {
            khItems[i] = provinces[i].getKhName();
            enItems[i] = provinces[i].getEnName();
            String kh = provinces[i].getKhName();
            String en = provinces[i].getEnName();
            int count = provinces[i].getItemSelectedCount();
            provinceList.add(new JsonData(kh, en, count));
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, khItems);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        provinceSelection.setAdapter(dataAdapter);
        provinceSelection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                khName = provinceList.get(position).getKhName();
                enName = provinceList.get(position).getEnName();
                plateName = khName + "-" + enName;
                enNameView.setText(enName);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    public String ReadFromfile(String fileName, Context context) {
        StringBuilder returnString = new StringBuilder();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;
        try {
            fIn = context.getResources().getAssets()
                    .open(fileName, Context.MODE_WORLD_READABLE);
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);
            String line = "";
            while ((line = input.readLine()) != null) {
                returnString.append(line);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (isr != null)
                    isr.close();
                if (fIn != null)
                    fIn.close();
                if (input != null)
                    input.close();
            } catch (Exception e2) {
                e2.getMessage();
            }
        }
        return returnString.toString();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.check_in_plate_car:
                onSubmitCarPlateData();
                break;
            case R.id.new_record_btn_car:
                newParkingRecord();
                break;
            case R.id.view_report_btn_car:
                Toast.makeText(getContext(), "clicked", Toast.LENGTH_SHORT).show();
                break;
            case R.id.print_report_btn_car:
                Toast.makeText(getContext(), "clicked", Toast.LENGTH_SHORT).show();
                break;
        }

    }

    private void newParkingRecord() {
        carPlateData = new Report();
        handleControlReportView(false);
    }

    private void onSubmitCarPlateData() {
        String checkText = String.valueOf(editTextPlateNumber.getText());
        if (checkText.length() > 8 || checkText.length() < 7) {
            editTextPlateNumber.setError("check this again");
        } else {
            handlePlateData();
            handleControlReportView(true);
        }
    }

    private String name, plateNum;
    private String checkedInBy, checkedOutBy;
    private String parkingLotId;
    private int ticketPrice;
    private String vehicleType = "Car";

    private Report parkingMotorReport;

    private void handlePlateData() {

        String phoneNumber = mAuth.getPhoneNumber();
        phoneNumber = phoneNumber.replace("+855", "0");
        checkedOutBy = "";
        final String finalPhoneNumber = phoneNumber;
        databaseReference.child("parkerInformation").child(phoneNumber).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                checkedInBy = finalPhoneNumber + String.valueOf(dataSnapshot.child("parkerName").getValue());
                parkingLotId = String.valueOf(dataSnapshot.child("workingAt").getValue());
                getPriceTicket(parkingLotId);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        Log.d("parkinglotid", "handlePlateData: " + parkingLotId);

        name = plateName;
        plateNum = String.valueOf(editTextPlateNumber.getText()).toUpperCase();

        Log.d("plateNumber", "handlePlateData: " + plateNum);
    }

    private void getPriceTicket(final String parkingLotId) {
        databaseReference.child("parkingLots").child(parkingLotId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ticketPrice = Integer.parseInt(String.valueOf(dataSnapshot.child("carPrice").getValue()));
                parkingMotorReport = new Report(checkedInBy, checkedOutBy, parkingLotId, name, plateNum, ticketPrice, vehicleType);
                uploadPlateData(parkingMotorReport);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void uploadPlateData(Report parkingMotorReport) {
        DatabaseReference parkingDetailReference = databaseReference.child("parkingDetails").child(parkingMotorReport.getParkingLot()).push();
        parkingDetailReference.child("checkedInBy").setValue(parkingMotorReport.getCheckedInBy());
        parkingDetailReference.child("checkedOutBy").setValue(parkingMotorReport.getCheckedOutBy());
        parkingDetailReference.child("plateID").setValue(parkingMotorReport.getPlateID());
        parkingDetailReference.child("plateName").setValue(parkingMotorReport.getPlateNameE());
        parkingDetailReference.child("vehicleType").setValue(parkingMotorReport.getVehicleType());
        parkingDetailReference.child("ticketPrice").setValue(parkingMotorReport.getTicketPrice());
        parkingDetailReference.child("ticketPriceOvernight").setValue(parkingMotorReport.getTicketPriceOvernight());
        parkingDetailReference.child("timestampCheckedIn").setValue(ServerValue.TIMESTAMP);
        parkingDetailReference.child("timestampCheckedOut").setValue("");

    }

    private void handleControlReportView(boolean isCompleted) {
        if (isCompleted) {
            uploadDataButton.setVisibility(View.GONE);
            reportControlView.setVisibility(View.VISIBLE);
            provinceSelection.setEnabled(false);
            editTextPlateNumber.setEnabled(false);
        } else {
            provinceSelection.setEnabled(true);
            editTextPlateNumber.setEnabled(true);
            uploadDataButton.setVisibility(View.VISIBLE);
            reportControlView.setVisibility(View.GONE);
            editTextPlateNumber.setText("");
        }
    }

}
