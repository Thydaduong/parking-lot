package com.lot.parking.theparkeradmin.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.lot.parking.theparkeradmin.AddParkingLotActivity;
import com.lot.parking.theparkeradmin.R;
import com.lot.parking.theparkeradmin.adapter.ParkingLotAdapter;
import com.lot.parking.theparkeradmin.model.ParkingLot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class ParkingLotFragment extends Fragment implements View.OnClickListener {

    List<ParkingLot> parkingLotList = new ArrayList<>();
    RecyclerView recyclerView;
    ParkingLotAdapter adapter;

    public ParkingLotFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_parking_lot, container, false);


        //handle data to recyclerView
        recyclerView = view.findViewById(R.id.id_rv_parking_lot_container);
        handleParkingLotData();
        adapter = new ParkingLotAdapter(parkingLotList);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        view.findViewById(R.id.fab_add_parking_lot).setOnClickListener(this);

        return view;
    }


    //    public ParkingLot(String parkingName, String parkingAddress, String numberOfParker, String availableTime, String ticketPrice) {
    private void handleParkingLotData() {


        DatabaseReference parkerListRef = FirebaseDatabase.getInstance().getReference().child("parkingLots");
        Query queryParker = parkerListRef.orderByChild("adminId").equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid());
//        queryParker.orderByChild("parkingLotName");
        queryParker.addValueEventListener(new ValueEventListener() {
            ProgressDialog dialog = ProgressDialog.show(getContext(), "",
                    "Loading. Please wait...", true);
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                parkingLotList.clear();
                for (DataSnapshot parkerSnapshot : dataSnapshot.getChildren()) {

                    Map<String, String> parkerData = new HashMap<String, String>();
                    String parkingName = parkerSnapshot.child("parkingLotName").getValue(String.class);
                    String parkingAddress = parkerSnapshot.child("parkingLotAddress").getValue(String.class);
                    String carPrice = String.valueOf(parkerSnapshot.child("carPrice").getValue() + " KHR");
                    String motoDefaultPrice = String.valueOf(parkerSnapshot.child("motoDefaultPrice").getValue() + " KHR");
                    String motoOvernightPrice = String.valueOf(parkerSnapshot.child("motoOvernightPrice").getValue() + " KHR");
                    String countParker = "4 staff";
                    String availableTime = String.valueOf(parkerSnapshot.child("availableTime").getValue()+"");


                    parkerData.put("parkingName", parkingName);
                    parkerData.put("availableTime", availableTime);
                    parkerData.put("parkingLotAddress", parkingAddress);
                    parkerData.put("carPrice", carPrice);
                    parkerData.put("motoDefaultPrice", motoDefaultPrice);
                    parkerData.put("motoOvernightPrice", motoOvernightPrice);
                    parkerData.put("parkerCount", countParker);

                    ParkingLot lot = new ParkingLot(parkerData);
                    parkingLotList.add(lot);
                    adapter.notifyDataSetChanged();
                }
                dialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("ids", databaseError.toString() + "");
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.fab_add_parking_lot){
            startActivity(new Intent(getContext(), AddParkingLotActivity.class));
        }
    }


}
