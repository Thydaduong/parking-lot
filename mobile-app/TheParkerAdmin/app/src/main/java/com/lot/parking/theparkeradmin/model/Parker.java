package com.lot.parking.theparkeradmin.model;

import java.util.Map;

public class Parker {

    String parkerName, workingAt, workingTime, profileUrl;

    public Parker() {}

    public Parker(String parkerName, String workingAt, String workingTime, String profileUrl) {
        this.parkerName = parkerName;
        this.workingAt = workingAt;
        this.workingTime = workingTime;
        this.profileUrl = profileUrl;
    }

    public  Parker(Map<String, String> data) {
        this.parkerName = data.get("parkerName");
        this.workingAt = data.get("workingAt");
        this.workingTime = data.get("workingTime");
        this.profileUrl = data.get("addedAt");
    }



    public String getParkerName() {
        return parkerName;
    }

    public String getWorkingPlace() {
        return workingAt;
    }

    public String getWorkingTime() {
        return workingTime;
    }

    public String getProfileUrl() {
        return profileUrl;
    }
}
