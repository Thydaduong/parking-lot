package com.lot.parking.theparkeradmin;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.lot.parking.theparkeradmin.adapter.PriceCaseAdapter;
import com.lot.parking.theparkeradmin.model.PriceCase;

import java.util.ArrayList;
import java.util.List;

public class AddParkingLotCaseActivity extends AppCompatActivity {

    private String toolbarTitle = "Add Parking Lot";
    private List<PriceCase> priceCaseList;
    private PriceCaseAdapter priceCaseAdapter;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView rv_car_price;
    private ImageView btn_add_car_price_case;

    private Button submitParkingLotInfo;
    private EditText etLotName, etLotAddress, etMotoTicketPrice, etMotoOvernightPrice;
//    private EditText etCarOvernightPrice

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_parking_lot_case);

        Toolbar toolbar = findViewById(R.id.add_parking_lot_toolbar);
        toolbar.setTitle(toolbarTitle);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        rv_car_price = findViewById(R.id.rv_car_price);
        btn_add_car_price_case = findViewById(R.id.add_price_case);
        init();
        initEvent();

        submitParkingLotInfo = findViewById(R.id.btn_submit_parking_lot_info);
        handleClickSubmit();
    }

    private void handleClickSubmit() {
        submitParkingLotInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleParkingLotInformation();
            }
        });
    }

    private void handleParkingLotInformation() {

    }

    private void init() {
        priceCaseList = new ArrayList<>();
        priceCaseList.add(new PriceCase("05:00", "21:00", "500"));
        priceCaseAdapter = new PriceCaseAdapter(this, priceCaseList,onDeleteItemResponse);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_car_price.setNestedScrollingEnabled(false);
        rv_car_price.setLayoutManager(linearLayoutManager);
        rv_car_price.setHasFixedSize(false);
        rv_car_price.setAdapter(priceCaseAdapter);
    }

    private void initEvent() {
        btn_add_car_price_case.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<PriceCase> priceCases = new ArrayList<>();
                priceCases.add(new PriceCase("05:00", "21:00", "500"));
                priceCaseList.addAll(priceCases);
                priceCaseAdapter.notifyItemInserted(priceCaseList.size() - 1);

                if(priceCaseList.size() == 5){
                    btn_add_car_price_case.setVisibility(View.GONE);
                }
            }
        });
    }

    private OnDeleteItemResponse onDeleteItemResponse = new OnDeleteItemResponse() {
        @Override
        public void onResponded(Boolean aBoolean) {
            if(aBoolean){
                if(priceCaseList.size() != 5){
                    btn_add_car_price_case.setVisibility(View.VISIBLE);
                }
            }
        }
    };
}
