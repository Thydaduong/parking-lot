package com.lot.parking.theparkeradmin;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.lot.parking.theparkeradmin.model.ParkerInformation;
import com.lot.parking.theparkeradmin.model.ParkingLot;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddParkerActivity extends AppCompatActivity implements View.OnClickListener, TextWatcher {

    private static final int PICK_IMAGE_REQUEST = 123;
    private android.support.v7.widget.Toolbar toolbar;
    private String toolbarTitle = "Add Parker";
    private EditText tvParkerName, tvParkerAddress, tvPhoneNumber;
    private TextView tvPhoneError;
    private ParkerInformation parkerInformation;
    private Uri filePath;

    private SimpleDateFormat dateTimeFormat;
    private Calendar calendar;

    private FirebaseUser admin;
    private FirebaseStorage storage;
    private StorageReference storageReference;

    private ImageView icCamera;
    private CircleImageView parkerProfileImg;

    private ParkingLot parkingLot;
    private String parkingLotId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_parker);

        icCamera = findViewById(R.id.ic_camera_id);
        parkerProfileImg = findViewById(R.id.parker_profile_img);

        toolbar = findViewById(R.id.parker_toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitle(toolbarTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tvPhoneError = findViewById(R.id.tv_phone_error);
        handleParkingLot();
        tvParkerName = findViewById(R.id.tv_parker_name);
        tvParkerAddress = findViewById(R.id.tv_parker_address);
        tvPhoneNumber = findViewById(R.id.tv_phone_number);
        findViewById(R.id.add_parker_info_btn).setOnClickListener(this);
        findViewById(R.id.set_profile_btn).setOnClickListener(this);
        findViewById(R.id.set_profile_btn).setOnClickListener(this);

        tvPhoneNumber.addTextChangedListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.add_parker_info_btn) {
            handleParkerInfo();
        } else if (v.getId() == R.id.set_profile_btn) {
            setParkerProfile();
        }
    }

    private void setParkerProfile() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        icCamera.setVisibility(View.VISIBLE);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                parkerProfileImg.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }

            icCamera.setVisibility(View.GONE);
        }
    }

    private void submitParkerInfo() {
        String parkerName, parkerAddress, phoneNumber, workingAt, adminId, addedAt, lastUpdated;

//        handleParkerInfo();
        DatabaseReference parkerRootRef = FirebaseDatabase.getInstance().getReference("parkerInformation");
        final DatabaseReference reference = parkerRootRef.child(parkerInformation.getPhoneNumber());
        parkerRootRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (!dataSnapshot.hasChild(parkerInformation.getPhoneNumber())){
                    reference.child("parkerName").setValue(parkerInformation.getParkerName());
                    reference.child("adminId").setValue(parkerInformation.getAdminId());
                    reference.child("workingAt").setValue(parkerInformation.getWorkingAt());
                    reference.child("addedAt").setValue(parkerInformation.getAddedAt());
                    reference.child("lastUpdated").setValue(parkerInformation.getLastUpdated());
                    finish();

                } else {
                    tvPhoneNumber.setBackground(getDrawable(R.drawable.rectangle_edittext_error));
                    tvPhoneError.setText("Phone number is already exist! Enter the new once.");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


    public void handleParkingLot() {
        final DatabaseReference parkingLotRef = FirebaseDatabase.getInstance().getReference().child("parkingLots");
//        parkingLotRef.orderByChild("adminId").equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid());
        final Query query = parkingLotRef.orderByChild("adminId").equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid());
//        storage = FirebaseStorage.getInstance();
//        storageReference = storage.getReference();
        query.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final List<String> parkingName = new ArrayList<String>();
                final List<String> lotId = new ArrayList<>();
                parkingName.add("Not Selected");
                lotId.add("");
                for (DataSnapshot parkingSnapshot : dataSnapshot.getChildren()) {
                    parkingName.add(parkingSnapshot.child("parkingLotName").getValue(String.class));
                    lotId.add(parkingSnapshot.getKey());
                }

                final Spinner parkingLotChoices = findViewById(R.id.parking_lot_choices);
                ArrayAdapter<String> parkingLotAdapter = new ArrayAdapter<>(AddParkerActivity.this, android.R.layout.simple_spinner_item, parkingName);
                parkingLotAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                parkingLotChoices.setAdapter(parkingLotAdapter);


                parkingLotChoices.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        parkingLotId = lotId.get(position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    @SuppressLint("SimpleDateFormat")
    private void handleParkerInfo() {
        String parkerName, parkerAddress, phoneNumber, workingAt, adminId, addedAt, lastUpdated;
        Calendar calendar;

        admin = FirebaseAuth.getInstance().getCurrentUser();

        adminId = admin != null ? admin.getUid() : "";
        parkerName = tvParkerName.getText().toString();
        parkerAddress = tvParkerAddress.getText().toString();
        phoneNumber = tvPhoneNumber.getText().toString();

        workingAt = parkingLotId;

        dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        calendar = Calendar.getInstance();
        addedAt = dateTimeFormat.format(calendar.getTime());
        lastUpdated = addedAt;

//        parkerName, parkerAddress, phoneNumber, workingAt, adminId, addedAt, lastUpdated

        if (!parkerName.equals("") && !parkerAddress.equals("") && !phoneNumber.equals("")) {
            parkerInformation = new ParkerInformation(parkerName, parkerAddress, phoneNumber, workingAt, adminId, addedAt, lastUpdated);
            submitParkerInfo();
            Toast.makeText(getApplicationContext(), "Parker Added", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "be sure that, you have fill all fields.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        tvPhoneNumber.setBackground(getDrawable(R.drawable.rectangle_edittext));
        tvPhoneError.setText("");
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}