package com.lot.parking.theparkeradmin.model;

public class PriceCase {
    private String fromTime;
    private String toTime;
    private String price;

    public PriceCase(String fromTime, String toTime, String price) {
        this.fromTime = fromTime;
        this.toTime = toTime;
        this.price = price;
    }

    public String getFromTime() {
        if(fromTime == null || fromTime.equalsIgnoreCase("null"))
            fromTime = "";
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        if(toTime == null || toTime.equalsIgnoreCase("null"))
            toTime = "";
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getPrice() {
        if(price == null || price.equalsIgnoreCase("null"))
            price = "";
            return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
