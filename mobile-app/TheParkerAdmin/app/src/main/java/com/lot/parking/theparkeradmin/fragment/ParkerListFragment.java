package com.lot.parking.theparkeradmin.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.lot.parking.theparkeradmin.AddParkerActivity;
import com.lot.parking.theparkeradmin.R;
import com.lot.parking.theparkeradmin.adapter.ParkerAdapter;
import com.lot.parking.theparkeradmin.model.Parker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ParkerListFragment extends Fragment implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    List<Parker> parkerList = new ArrayList<>();
    RecyclerView recyclerView;
    ParkerAdapter adapter;

    public ParkerListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_parker_list, container, false);

        handleParkerData();

        Log.d("ddd", "logdjfdsljfsl");

        recyclerView = view.findViewById(R.id.id_rv_parker_container);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = new ParkerAdapter(parkerList);
        recyclerView.setAdapter(adapter);

        view.findViewById(R.id.fab_add_parker_info).setOnClickListener(this);
        return view;
    }



    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    private void handleParkerData() {

        final DatabaseReference parkerListRef = FirebaseDatabase.getInstance().getReference().child("parkerInformation");
        final Query queryParker = parkerListRef.orderByChild("adminId").equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid());
        final ProgressDialog dialog = ProgressDialog.show(getContext(), "",
                "Loading. Please wait...", true);
        queryParker.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                parkerList.clear();

                for (DataSnapshot parkerSnapshot : dataSnapshot.getChildren()
                        ) {

                    final Map<String, String> data = new HashMap<String, String>();
                    final String parkerName = parkerSnapshot.child("parkerName").getValue(String.class);
                    final String workingAtLotId = parkerSnapshot.child("workingAt").getValue(String.class);
                    final String workingTime = parkerSnapshot.child("workingTime").getValue(String.class);
                    final String addedAt = parkerSnapshot.child("addedAt").getValue(String.class);
                    if (workingAtLotId != null) {
                        FirebaseDatabase.getInstance().getReference().child("parkingLots").child(workingAtLotId).addListenerForSingleValueEvent(new ValueEventListener() {

                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                String lotName = dataSnapshot.child("parkingLotName").getValue(String.class);

                                data.put("parkerName", parkerName);
                                data.put("workingAt", lotName);
                                data.put("workingTime", workingTime);
                                data.put("addedAt", addedAt);

                                Parker parker = new Parker(data);

                                parkerList.add(parker);

                                adapter.notifyDataSetChanged();
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                adapter.notifyDataSetChanged();
                            }
                        });
                    }
                }
                dialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("ids", databaseError.toString() + "");
            }
        });

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.fab_add_parker_info) {
            startActivity(new Intent(getContext(), AddParkerActivity.class));
        }
    }

}
