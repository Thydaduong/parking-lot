package com.lot.parking.theparkeradmin.model;

public class DashBoard {

    private String boardTitle, profit, parkCounting, ticketPrice, parkerNumber;

    public DashBoard(String boardTitle, String profit, String parkCounting, String ticketPrice, String parkerNumber) {
        this.boardTitle = boardTitle;
        this.profit = profit;
        this.parkCounting = parkCounting;
        this.ticketPrice = ticketPrice;
        this.parkerNumber = parkerNumber;
    }

    public String getBoardTitle() {
        return boardTitle;
    }

    public String getProfit() {
        return profit;
    }

    public String getParkCounting() {
        return parkCounting;
    }

    public String getTicketPrice() {
        return ticketPrice;
    }

    public String getParkerNumber() {
        return parkerNumber;
    }
}
