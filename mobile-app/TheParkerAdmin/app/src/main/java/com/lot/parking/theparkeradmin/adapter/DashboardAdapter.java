package com.lot.parking.theparkeradmin.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lot.parking.theparkeradmin.R;
import com.lot.parking.theparkeradmin.model.DashBoard;

import java.util.ArrayList;
import java.util.List;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.DashboardViewHolder> {

    List<DashBoard> dashBoards = new ArrayList<>();
    private String strLotName, strProfitAmount, strTicketPrice, strParkingCounting, strParkerCounting;


    public DashboardAdapter(List<DashBoard> dashBoards) {
        this.dashBoards = dashBoards;
    }

    public class DashboardViewHolder extends RecyclerView.ViewHolder {
        public TextView tvLotName, tvProfitAmount, tvTicketPrice, tvParkingCounting, tvParkerNumber;

        public DashboardViewHolder(View itemView) {
            super(itemView);
            tvLotName = itemView.findViewById(R.id.id_tv_parking_lot_name);
            tvProfitAmount = itemView.findViewById(R.id.id_tv_amount_profit);
            tvTicketPrice = itemView.findViewById(R.id.id_motor_price);
            tvParkerNumber = itemView.findViewById(R.id.id_count_parker);
            tvParkingCounting = itemView.findViewById(R.id.id_parking_counting_motor);
        }
    }

    @NonNull
    @Override
    public DashboardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_parking_board, parent, false);
        return new DashboardViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DashboardViewHolder holder, int position) {
        DashBoard dashBoard = dashBoards.get(position);
        holder.tvLotName.setText(dashBoard.getBoardTitle());
//        holder.tvProfitAmount.setText(dashBoard.getProfit());
//        holder.tvTicketPrice.setText(dashBoard.getTicketPrice());
//        holder.tvParkingCounting.setText(dashBoard.getParkCounting());
//        holder.tvParkerNumber.setText(dashBoard.getParkerNumber());
    }

    @Override
    public int getItemCount() {
        return dashBoards.size();
    }

}
