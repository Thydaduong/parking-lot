package com.lot.parking.theparkeradmin.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.lot.parking.theparkeradmin.R;
import com.lot.parking.theparkeradmin.adapter.DashboardAdapter;
import com.lot.parking.theparkeradmin.model.DashBoard;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragement extends Fragment implements AdapterView.OnItemSelectedListener {

    RecyclerView recyclerView;
    DashboardAdapter mAdapter;
    List<DashBoard> dashBoards = new ArrayList<>();

    private DatabaseReference databaseRef;
    private FirebaseUser firebaseUser;
    String parkingLotsId;
    String timestampCheckedIn;
    public DashboardFragement() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragement_dashboard, container, false);

        // Spinner element
        Spinner spinner = (Spinner) view.findViewById(R.id.spinner_filter_date);
        // Spinner click listener
        spinner.setOnItemSelectedListener(this);

        spinnerFilterViewByDate(spinner);


        // passing data to recyclerView
        recyclerView = view.findViewById(R.id.id_rv_dashboard_container);

        prepareDashboardData();

        mAdapter = new DashboardAdapter(dashBoards);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);




        return view;
    }

    private void spinnerFilterViewByDate(Spinner spinner) {

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Today");
        categories.add("Yesterday");
        categories.add("This week");
        categories.add("Last Week");
        categories.add("This Month");
        categories.add("Last Month");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(this);
    }

    private void prepareDashboardData() {
//        dashBoards.add(new DashBoard( "RUPP Building B", "1,250,000 KHR", "1,250 times", "1000 KHR","4 staff"));
//        dashBoards.add(new DashBoard( "RUPP Building A", "2,250,000 KHR", "2,250 times", "1000 KHR","4 staff"));
//        dashBoards.add(new DashBoard( "RUPP IFL", "1,000,000 KHR", "1,000 times", "1000 KHR","4 staff"));
//        dashBoards.add(new DashBoard( "RUPP CKCC", "250,000 KHR", "250 times", "1000 KHR","4 staff"));
//        dashBoards.add(new DashBoard( "ACE", "1,500,000 KHR", "1,500 times", "1000 KHR","4 staff"));
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        databaseRef.child("parkingLots").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    if (snapshot.child("adminId").getValue().equals(firebaseUser.getUid())) {
                        dashBoards.add(new DashBoard(String.valueOf(snapshot.child("parkingLotName").getValue())," "," ","5","1000"));
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


}
