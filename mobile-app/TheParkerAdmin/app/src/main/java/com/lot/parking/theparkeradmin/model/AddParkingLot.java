package com.lot.parking.theparkeradmin.model;

public class AddParkingLot {
    private String adminId, parkingLotName, parkingLotAddress;
    private Double motoDefaultPrice, motoOvernightPrice, carTicketPrice;
    private String availableTime;
    public AddParkingLot() {
    }

    public AddParkingLot(String adminId, String parkingLotName, String parkingLotAddress, String availableTime, Double motoDefaultPrice, Double motoOvernightPrice, Double carTicketPrice) {
        this.adminId = adminId;
        this.parkingLotName = parkingLotName;
        this.parkingLotAddress = parkingLotAddress;
        this.availableTime =availableTime;
        this.motoDefaultPrice = motoDefaultPrice;
        this.motoOvernightPrice = motoOvernightPrice;
        this.carTicketPrice = carTicketPrice;
    }

    public String getAdminId() {
        return adminId;
    }

    public String getParkingLotName() {
        return parkingLotName;
    }

    public String getParkingLotAddress() {
        return parkingLotAddress;
    }

    public String getAvailableTime() {
        return availableTime;
    }

    public Double getMotoDefaultPrice() {
        return motoDefaultPrice;
    }

    public Double getMotoOvernightPrice() {
        return motoOvernightPrice;
    }

    public Double getCarTicketPrice() {
        return carTicketPrice;
    }
}

