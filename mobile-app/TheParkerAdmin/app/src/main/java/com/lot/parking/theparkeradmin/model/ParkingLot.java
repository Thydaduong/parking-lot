package com.lot.parking.theparkeradmin.model;

import java.util.Map;

public class ParkingLot {
    private String parkingName, parkingAddress, numberOfParker, availableTime, lotId;
    private String motorDefaultPrice, motorOvernightPrice, carPrice;
    public ParkingLot() {
    }

    public ParkingLot(String parkingName, String parkingAddress, String numberOfParker, String availableTime, String lotId, String motorDefaultPrice, String motorOvernightPrice, String carPrice) {
        this.parkingName = parkingName;
        this.parkingAddress = parkingAddress;
        this.numberOfParker = numberOfParker;
        this.availableTime = availableTime;
        this.lotId = lotId;
        this.motorDefaultPrice = motorDefaultPrice;
        this.motorOvernightPrice = motorOvernightPrice;
        this.carPrice = carPrice;
    }

    public ParkingLot(String parkingName, String lotId) {
        this.parkingName = parkingName;
        this.lotId = lotId;
    }

    public ParkingLot(Map<String, String> parkerData) {
        this.parkingName =parkerData.get("parkingName");
        this.availableTime = parkerData.get("availableTime");
        this.parkingAddress = parkerData.get("parkingLotAddress");
        this.motorDefaultPrice = parkerData.get("motoDefaultPrice");
        this.motorOvernightPrice = parkerData.get("motoOvernightPrice");
        this.carPrice = parkerData.get("carPrice");
        this.numberOfParker = parkerData.get("parkerCount");
    }

    public String getParkingName() {
        return parkingName;
    }

    public String getParkingAddress() {
        return parkingAddress;
    }

    public String getNumberOfParker() {
        return numberOfParker;
    }

    public String getAvailableTime() {
        return availableTime;
    }

    public String getLotId() {
        return lotId;
    }

    public String getMotorDefaultPrice() {
        return motorDefaultPrice;
    }

    public String getMotorOvernightPrice() {
        return motorOvernightPrice;
    }

    public String getCarPrice() {
        return carPrice;
    }
}
