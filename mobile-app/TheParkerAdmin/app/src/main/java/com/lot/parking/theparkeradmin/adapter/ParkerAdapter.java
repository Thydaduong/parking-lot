package com.lot.parking.theparkeradmin.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lot.parking.theparkeradmin.R;
import com.lot.parking.theparkeradmin.model.Parker;

import java.util.ArrayList;
import java.util.List;

public class ParkerAdapter extends RecyclerView.Adapter<ParkerAdapter.ParkerRecyclerViewHolder>{
    private List<Parker> parkerList = new ArrayList<>();

    public ParkerAdapter(List<Parker> parkerList) {
        this.parkerList = parkerList;
    }

    @NonNull
    @Override
    public ParkerAdapter.ParkerRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_parker, parent, false);

        return new ParkerRecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ParkerAdapter.ParkerRecyclerViewHolder holder, int position) {
        Parker parker = parkerList.get(position);
        holder.tvParkerName.setText(parker.getParkerName());
        holder.tvParkerWorkingPlace.setText(parker.getWorkingPlace());
        holder.tvParkerWorkingTime.setText(parker.getWorkingTime());
    }

    @Override
    public int getItemCount() {
        return parkerList.size();
    }

    public class ParkerRecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView tvParkerName, tvParkerWorkingPlace, tvParkerWorkingTime;
        public ParkerRecyclerViewHolder(View itemView) {
            super(itemView);

            tvParkerName = itemView.findViewById(R.id.id_parker_name);
            tvParkerWorkingPlace = itemView.findViewById(R.id.id_parker_working_place);
            tvParkerWorkingTime = itemView.findViewById(R.id.id_parker_working_time);
        }
    }
}
