package com.lot.parking.theparkeradmin.adapter;

import android.app.TimePickerDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.lot.parking.theparkeradmin.OnDeleteItemResponse;
import com.lot.parking.theparkeradmin.R;
import com.lot.parking.theparkeradmin.model.PriceCase;

import java.util.Calendar;
import java.util.List;

public class PriceCaseAdapter extends RecyclerView.Adapter<PriceCaseAdapter.ViewHolder> {

    private Context mContext;
    private List<PriceCase> mPriceCases;
    private OnDeleteItemResponse mOnDeleteItemResponse;

    public PriceCaseAdapter(Context mContext, List<PriceCase> mPriceCases, OnDeleteItemResponse mOnDeleteItemResponse) {
        this.mContext = mContext;
        this.mPriceCases = mPriceCases;
        this.mOnDeleteItemResponse = mOnDeleteItemResponse;
    }

    @NonNull
    @Override
    public PriceCaseAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.cell_price_case_car, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final PriceCaseAdapter.ViewHolder holder, final int position) {
        if (mPriceCases.size() == 1) {
            holder.iv_close.setVisibility(View.INVISIBLE);
        } else {
            holder.iv_close.setVisibility(View.VISIBLE);
        }
        holder.tv_fromTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(mContext, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String hour = (selectedHour < 10) ? "0" + selectedHour : String.valueOf(selectedHour);
                        String minute = (selectedMinute < 10) ? "0" + selectedMinute : String.valueOf(selectedMinute);
                        String time = hour + ":" + minute;
                        holder.tv_fromTime.setText(time);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });
        holder.tv_toTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(mContext, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String hour = (selectedHour < 10) ? "0" + selectedHour : String.valueOf(selectedHour);
                        String minute = (selectedMinute < 10) ? "0" + selectedMinute : String.valueOf(selectedMinute);
                        String time = hour + ":" + minute;
                        holder.tv_fromTime.setText(time);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });
        holder.et_price.setText(mPriceCases.get(position).getPrice());
        holder.iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPriceCases.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeRemoved(position, mPriceCases.size());
                Log.d("deleteId", position + "");
                if (mOnDeleteItemResponse != null) {
                    mOnDeleteItemResponse.onResponded(true);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        int size = (mPriceCases == null) ? 0 : mPriceCases.size();
        return size;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_fromTime, tv_toTime;
        private EditText et_price;
        private ImageView iv_close;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_fromTime = itemView.findViewById(R.id.car_start_time);
            tv_toTime = itemView.findViewById(R.id.car_end_time);
            et_price = itemView.findViewById(R.id.car_ticket_price);
            iv_close = itemView.findViewById(R.id.btn_close);

        }
    }
}
