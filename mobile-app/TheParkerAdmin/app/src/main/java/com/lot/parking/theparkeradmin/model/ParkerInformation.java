package com.lot.parking.theparkeradmin.model;

public class ParkerInformation {

    private String parkerName, parkerAddress, phoneNumber, workingAt, adminId, addedAt, lastUpdated;

    public ParkerInformation() {}

    public ParkerInformation(String parkerName, String parkerAddress, String phoneNumber, String workingAt, String adminId, String addedAt, String lastUpdated) {
        this.parkerName = parkerName;
        this.parkerAddress = parkerAddress;
        this.phoneNumber = phoneNumber;
        this.workingAt = workingAt;
        this.adminId = adminId;
        this.addedAt = addedAt;
        this.lastUpdated = lastUpdated;
    }

    public String getParkerName() {
        return parkerName;
    }

    public String getParkerAddress() {
        return parkerAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getWorkingAt() {
        return workingAt;
    }

    public String getAdminId() {
        return adminId;
    }

    public String getAddedAt() {
        return addedAt;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }
}
