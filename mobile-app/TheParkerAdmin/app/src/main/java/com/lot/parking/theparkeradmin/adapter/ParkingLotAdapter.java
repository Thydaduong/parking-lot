package com.lot.parking.theparkeradmin.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.lot.parking.theparkeradmin.R;
import com.lot.parking.theparkeradmin.model.ParkingLot;

import java.util.List;

public class ParkingLotAdapter extends RecyclerView.Adapter<ParkingLotAdapter.ParkingLotViewHolder>{

    List<ParkingLot> parkingLots;

    Context context;

    public ParkingLotAdapter(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    @NonNull
    @Override
    public ParkingLotViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.card_parking_lot, parent, false);

        return new ParkingLotViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ParkingLotViewHolder holder, int position) {
        ParkingLot parkingLot = parkingLots.get(position);
        holder.tvTitle.setText(parkingLot.getParkingName());
        holder.tvAddress.setText(parkingLot.getParkingAddress());
        holder.tvAvailableTime.setText(parkingLot.getAvailableTime());
        holder.tvMotorPrice.setText(parkingLot.getMotorDefaultPrice());
        holder.tvMotorOvernigthPrice.setText(parkingLot.getMotorOvernightPrice());
        holder.tvCarPrice.setText(parkingLot.getCarPrice());
        holder.tvParkerCount.setText(parkingLot.getNumberOfParker());
    }

    @Override
    public int getItemCount() {
        return parkingLots.size();
    }

    public class ParkingLotViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvTitle, tvAddress, tvAvailableTime, tvMotorPrice, tvMotorOvernigthPrice, tvCarPrice, tvParkerCount;

        public ParkingLotViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.id_tv_parking_lot_name);
            tvAddress = itemView.findViewById(R.id.id_lot_address);
            tvAvailableTime = itemView.findViewById(R.id.id_tv_available_time);
            tvMotorPrice = itemView.findViewById(R.id.id_tv_motor_price);
            tvMotorOvernigthPrice = itemView.findViewById(R.id.id_tv_price_per_night
            );
            tvCarPrice = itemView.findViewById(R.id.id_tv_car_price_car);
            tvParkerCount = itemView.findViewById(R.id.id_tv_parker_number);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if (position!= RecyclerView.NO_POSITION){
                ParkingLot parkingLot = parkingLots.get(position);
                Toast.makeText(context, "Lot:" + String.valueOf(parkingLot.getParkingName()), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
