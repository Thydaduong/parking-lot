package com.lot.parking.theparkeradmin;

import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.lot.parking.theparkeradmin.model.AddParkingLot;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AddParkingLotActivity extends AppCompatActivity implements View.OnClickListener {

    private AddParkingLot parkingLot;
    private EditText textParkingLotName, textParkingLotAddress;
    private EditText textMotoDefaultPrice, textMotoOvernightPrice, textCarPrice;
    private TextView tvFromTime, tvToTime;
    private CheckBox checkBox24h;
    private String toolbarTitle = "Add Parking Lot";

    private SimpleDateFormat dateTimeFormat;
    private Calendar calendar;

    private FirebaseUser admin;
    private DatabaseReference parkingLotRoot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_parking_lot);

        Toolbar toolbar = findViewById(R.id.add_parking_lot_toolbar);
        toolbar.setTitle(toolbarTitle);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        textParkingLotName = findViewById(R.id.parking_lot_name);
        textParkingLotAddress = findViewById(R.id.parking_lot_address);
        textMotoDefaultPrice = findViewById(R.id.moto_price_default);
        textMotoOvernightPrice = findViewById(R.id.moto_price_overnight);
        textCarPrice = findViewById(R.id.tv_car_ticket_price);

        tvFromTime = findViewById(R.id.get_from_time_btn);
        tvToTime = findViewById(R.id.get_to_time_btn);
        checkBox24h = findViewById(R.id.checkbox_24h);

        handleTime();
        findViewById(R.id.add_parking_lot_btn).setOnClickListener(this);

    }

    private void handleTime() {
        checkBox24h.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (checkBox24h.isChecked()) {

                    tvFromTime.setEnabled(false);
                    tvToTime.setEnabled(false);

                    tvFromTime.setTextColor(Color.parseColor("#AAAAAA"));
                    tvToTime.setTextColor(Color.parseColor("#AAAAAA"));
                    tvToTime.setBackground(getDrawable(R.drawable.rectangle_edittext_disbled));
                    tvFromTime.setBackground(getDrawable(R.drawable.rectangle_edittext_disbled));
                } else {
                    tvFromTime.setEnabled(true);
                    tvToTime.setEnabled(true);
                    tvToTime.setBackground(getDrawable(R.drawable.rectangle_edittext));
                    tvFromTime.setBackground(getDrawable(R.drawable.rectangle_edittext));
                    tvFromTime.setTextColor(Color.parseColor("#0079bf"));
                    tvToTime.setTextColor(Color.parseColor("#0079bf"));
                    tvFromTime.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Calendar mcurrentTime = Calendar.getInstance();
                            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                            int minute = mcurrentTime.get(Calendar.MINUTE);
                            TimePickerDialog mTimePicker;
                            mTimePicker = new TimePickerDialog(AddParkingLotActivity.this, new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                                    String hour = (selectedHour < 10) ? "0" + selectedHour : String.valueOf(selectedHour);
                                    String minute = (selectedMinute < 10) ? "0" + selectedMinute : String.valueOf(selectedMinute);
                                    String time = hour + ":" + minute;
                                    tvFromTime.setText(time);
                                }
                            }, hour, minute, true);//Yes 24 hour time
                            mTimePicker.setTitle("Select Time");
                            mTimePicker.show();
                        }
                    });
                    tvToTime.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Calendar mcurrentTime = Calendar.getInstance();
                            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                            int minute = mcurrentTime.get(Calendar.MINUTE);
                            TimePickerDialog mTimePicker;
                            mTimePicker = new TimePickerDialog(AddParkingLotActivity.this, new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                                    String hour = (selectedHour < 10) ? "0" + selectedHour : String.valueOf(selectedHour);
                                    String minute = (selectedMinute < 10) ? "0" + selectedMinute : String.valueOf(selectedMinute);
                                    String time = hour + ":" + minute;
                                    tvToTime.setText(time);
                                }
                            }, hour, minute, true);//Yes 24 hour time
                            mTimePicker.setTitle("Select Time");
                            mTimePicker.show();
                        }
                    });
                }
            }
        });
        if (checkBox24h.isChecked()) {

            tvFromTime.setEnabled(false);
            tvToTime.setEnabled(false);

            tvFromTime.setTextColor(Color.parseColor("#AAAAAA"));
            tvToTime.setTextColor(Color.parseColor("#AAAAAA"));
            tvToTime.setBackground(getDrawable(R.drawable.rectangle_edittext_disbled));
            tvFromTime.setBackground(getDrawable(R.drawable.rectangle_edittext_disbled));
        } else {
            tvFromTime.setEnabled(true);
            tvToTime.setEnabled(true);
            tvToTime.setBackground(getDrawable(R.drawable.rectangle_edittext));
            tvFromTime.setBackground(getDrawable(R.drawable.rectangle_edittext));
            tvFromTime.setTextColor(Color.parseColor("#0079bf"));
            tvToTime.setTextColor(Color.parseColor("#0079bf"));
            tvFromTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(AddParkingLotActivity.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            String hour = (selectedHour < 10) ? "0" + selectedHour : String.valueOf(selectedHour);
                            String minute = (selectedMinute < 10) ? "0" + selectedMinute : String.valueOf(selectedMinute);
                            String time = hour + ":" + minute;
                            tvFromTime.setText(time);
                        }
                    }, hour, minute, true);//Yes 24 hour time
                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();
                }
            });
            tvToTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(AddParkingLotActivity.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            String hour = (selectedHour < 10) ? "0" + selectedHour : String.valueOf(selectedHour);
                            String minute = (selectedMinute < 10) ? "0" + selectedMinute : String.valueOf(selectedMinute);
                            String time = hour + ":" + minute;
                            tvToTime.setText(time);
                        }
                    }, hour, minute, true);//Yes 24 hour time
                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.add_parking_lot_btn) {
            handleParkingLot();
        }
    }

    private void submitParkingLot(AddParkingLot parkingLot) {
        parkingLotRoot = FirebaseDatabase.getInstance().getReference("parkingLots").push();

//        handleParkingLot();
        parkingLotRoot.child("adminId").setValue(parkingLot.getAdminId());
        parkingLotRoot.child("parkingLotName").setValue(parkingLot.getParkingLotName());
        parkingLotRoot.child("parkingLotAddress").setValue(parkingLot.getParkingLotAddress());
        parkingLotRoot.child("timeStampCreate").setValue(ServerValue.TIMESTAMP);
        parkingLotRoot.child("timeStampUpdated").setValue(ServerValue.TIMESTAMP);
        parkingLotRoot.child("motoDefaultPrice").setValue(parkingLot.getMotoDefaultPrice());
        parkingLotRoot.child("motoOvernightPrice").setValue(parkingLot.getMotoOvernightPrice());
        parkingLotRoot.child("carPrice").setValue(parkingLot.getCarTicketPrice());
        parkingLotRoot.child("availableTime").setValue(parkingLot.getAvailableTime());
        finish();
    }

    @SuppressLint("SimpleDateFormat")
    private void handleParkingLot() {
        String adminId, parkingLotName, parkingLotAddress;
        Double motoDefaultPrice, motoOvernightPrice, carPrice;
        String availableTime;
        boolean is24h = checkBox24h.isChecked();
        availableTime = initTime(is24h);
        admin = FirebaseAuth.getInstance().getCurrentUser();

        adminId = admin != null ? admin.getUid() : "";

        dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        calendar = Calendar.getInstance();


        parkingLotName = textParkingLotName.getText().toString();
        parkingLotAddress = textParkingLotAddress.getText().toString();
        motoDefaultPrice = Double.parseDouble(textMotoDefaultPrice.getText().toString());
        motoOvernightPrice = Double.parseDouble(textMotoOvernightPrice.getText().toString());
        carPrice = Double.parseDouble(textCarPrice.getText().toString());

        parkingLot = new AddParkingLot(adminId, parkingLotName, parkingLotAddress, availableTime, motoDefaultPrice, motoOvernightPrice, carPrice);
        submitParkingLot(parkingLot);

    }

    private String initTime(boolean is24h) {
        if (is24h) {
            return "24h";
        } else {
            return tvFromTime.getText() + "-" + tvToTime.getText();
        }
    }
}
