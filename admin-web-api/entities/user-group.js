var db = require('../configs/db');
var CURRENT_TIMESTAMP = { toSqlString: function() { return 'CURRENT_TIMESTAMP()'; } };
const UserGroup = function () {
	this.id = null;
	this.name = "";
	this.can_read = 1;
	this.can_write = 0;
	this.can_modify = 0;
	this.can_delete = 0;
	this.created_at = CURRENT_TIMESTAMP;
	this.updated_at = CURRENT_TIMESTAMP;
	this.created_by = 0;
	this.updated_by = 0;

};
 
UserGroup.prototype.getAllUserGroups = function(conditions, callback) {
	let sqlString = `
		SELECT 
			user_groups.*,
			creators.name as created_by_name,
			updators.name as updated_by_name
		FROM 
			user_groups
		LEFT JOIN
			users as creators
			ON creators.id = user_groups.created_by
		LEFT JOIN
			users as updators
			ON updators.id = user_groups.updated_by
		ORDER BY
			user_groups.created_at
			DESC
		`;

	db.query(sqlString, (error, result) => {
		if (error)
			throw error;
		callback(null, result);
	});
}

UserGroup.prototype.createUserGroup = function(callback) {
	sqlString = `INSERT INTO user_groups (
		name, 
		can_read, 
		can_write, 
		can_modify, 
		can_delete,
		created_at,
		updated_at,
		created_by,
		updated_by
	) 
	VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)`;
	let values = [
		this.name,
		this.can_read,
		this.can_write,
		this.can_modify,
		this.can_delete,
		this.created_at,
		this.updated_at,
		this.created_by,
		this.updated_by
	];

	db.query(db.format(sqlString, values), (error, result) => {
		if (error) 
			throw error;
		callback(null, result);
	});
}

UserGroup.prototype.getOneUserGroup = function(callback) {
	let sqlString = `
		SELECT 
			user_groups.*,
			creators.name as created_by_name,
			updators.name as updated_by_name
		FROM 
			user_groups
		LEFT JOIN
			users as creators
			ON creators.id = user_groups.created_by
		LEFT JOIN
			users as updators
			ON updators.id = user_groups.updated_by
		WHERE 
			user_groups.id = ?
		`;
	db.query(sqlString, this.id, (error, result) => {
		if (error)
			throw error;

		let data = {};
		if (result.length)
			data = result[0];
		callback(null, data);
	});
}


UserGroup.prototype.updateUserGroup = function(callback) {
	sql_string = `UPDATE user_groups SET
		name = ?, 
		can_read = ?, 
		can_write = ?, 
		can_modify = ?, 
		can_delete = ?,
		updated_at = ?,
		updated_by = ?
	WHERE id= ? `;
	db.query ( sql_string, [
		this.name, 
		this.can_read,
		this.can_write,
		this.can_modify,
		this.can_delete,
		this.updated_at, 
		this.updated_by,
		this.id
	], (error, result) => {
		if (error) 
			throw error;
		callback(null, result);
	});
}


UserGroup.prototype.deleteUserGroup = function(callback) {
	sql_string = `DELETE FROM user_groups WHERE id= ? `;
	db.query ( sql_string, this.id, (error, result) => {
		if (error) 
			throw error;
		callback(null, result);
	});
}
module.exports  = new UserGroup();