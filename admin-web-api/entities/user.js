var db = require('../configs/db');
var CURRENT_TIMESTAMP = { toSqlString: function() { return 'CURRENT_TIMESTAMP()'; } };

const User = function() {
	this.id = "";
	this.name = "";
	this.email = "";
	this.phone = "";
	this.password = ""; 
	this.id = null;
	this.firstname = "";
	this.lastname = "";
	this.avatar = "";
	this.gender = "";
	this.dob = "";
	this.address = "";
	this.description = "";
	this.role_identifier = 0;
	this.created_at = CURRENT_TIMESTAMP;
	this.updated_at = CURRENT_TIMESTAMP;
	this.created_by = 0;
	this.updated_by = 0;
	this.user_id = 0;
	this.parking_lot_id = 0;
	this.status = 0;
};	

User.prototype.getOneUserByPermission = function(permisionNumber, callback) {
	const sql = "SELECT * FROM users WHERE role_identifier = ? ORDER BY created_at DESC";
	db.query(sql, permisionNumber, (error, result, field) => {
		if (error)
			throw error;
		callback(result);
	});
};

User.prototype.getOneUserById = function(user_id, callback) {
	const sql = "SELECT * FROM users WHERE id = ? LIMIT 1";
	db.query(sql, user_id, (error, result, field) => {
		if (error)
			throw error;
		callback(result[0]);
	});
};

User.prototype.getOneUserByName = function(name, callback) {
	const sql = "SELECT * FROM users WHERE name = ? LIMIT 1";
	db.query(sql, name, (error, result, field) => {
		if (error)
			throw error;
		callback(result[0]);
	});
};

User.prototype.getOneUserByEmail = function(email, callback) {
	const sql = "SELECT * FROM users WHERE email = ? LIMIT 1";
	db.query(sql, email, (error, result, field) => {
		if (error)
			throw error;
		callback(result[0]);
	});
};

User.prototype.createLotController = function(callback) {
	const userSqlString = `
		INSERT INTO users (
			role_identifier,
			name, 
			email, 
			phone, 
			password, 
			created_at, 
			updated_at,
			status
		)
		VALUES (?, ?, ?, ?, ?, ?, ?, ?)
	`;

	const userInfoSqlString = `
		INSERT INTO user_informations (
			firstname, 
			lastname, 
			avatar, 
			gender, 
			dob, 
			address, 
			description, 
			created_at, 
			updated_at, 
			created_by, 
			updated_by,
			user_id
		)
		VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
	`;


	const lotControllerSqlString = `
		INSERT INTO parking_lot_controllers (
			parking_lot_id,
			user_id, 
			created_at, 
			updated_at,
			created_by, 
			updated_by
		)
		VALUES (?, ?, ?, ?, ?, ?)
	`;


	const userValues = [
		this.role_identifier,
		this.name,
		this.email,
		this.phone,
		this.password,
		this.created_at,
		this.updated_at,
		this.status
	];

	const mysqlUser = db.format(userSqlString, userValues);
	db.query(mysqlUser, (error, result, field) => {
		if (error)
			throw error;
		this.user_id = result.insertId;
		// user info
		const userInfoValues = [
			this.firstname,
			this.lastname,
			this.avatar,
			this.gender,
			this.dob,
			this.address,
			this.description,
			this.created_at,
			this.updated_at,
			this.created_by,
			this.updated_by,
			this.user_id
		];

		const mysqlInfoUser = db.format(userInfoSqlString, userInfoValues);
		db.query(mysqlInfoUser, (error, result, field) => {
			if (error)
				throw error;

			const lotControllerValues = [
				this.parking_lot_id,
				this.user_id, 
				this.created_at, 
				this.updated_at,
				this.created_by, 
				this.updated_by
			];
			const mysqlLotController = db.format(lotControllerSqlString, lotControllerValues);
			db.query(mysqlLotController, (error, result, field) => {
				if (error)
					throw error;
				callback(result.insertId);
			});
		});

	});
};

User.prototype.getOneLotController = function(id, callback){
	let sqlString =`
		SELECT 
			users.*,
			user_informations.*,
			creators.name as created_by_name,
			updators.name as updated_by_name
		FROM 
			user_informations
		LEFT JOIN
			users as creators
			ON creators.id = user_informations.created_by
		LEFT JOIN
			users as updators
			ON updators.id = user_informations.updated_by
		LEFT JOIN
			users
			ON user_informations.user_id = users.id
		WHERE users.id = ?
	`;

	db.query(sqlString, id, (err, result) => {
		if (err) 
			throw err;
		callback(result);
	});
};


module.exports = new User();
 