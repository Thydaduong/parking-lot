var db = require('../configs/db');
var CURRENT_TIMESTAMP = { toSqlString: function() { return 'CURRENT_TIMESTAMP()'; } };
const Vehicle = function () {
	this.id = null;
	this.name = ""; 
	this.description = ""; 
	this.status = 0; 
	this.created_at = CURRENT_TIMESTAMP;
	this.updated_at = CURRENT_TIMESTAMP;
	this.created_by = 0;
	this.updated_by = 0;

};
 
Vehicle.prototype.getAllVehicleTypes = function(conditions, callback) {

	let sqlString = `
		SELECT 
			vehicle_types.*,
			creators.name as created_by_name,
			updators.name as updated_by_name
		FROM 
			vehicle_types
		LEFT JOIN
			users as creators
			ON creators.id = vehicle_types.created_by
		LEFT JOIN
			users as updators
			ON updators.id = vehicle_types.updated_by
	`;

	if (conditions) {
		if (conditions.search) {
			sqlString += ` WHERE vehicle_types.name LIKE '%${conditions.search}%'`;

		}
	}

	sqlString += ' ORDER BY vehicle_types.created_at DESC';
	

	db.query(sqlString, (error, result) => {
		if (error)
			throw error;
		callback(null, result);
	});
}

Vehicle.prototype.getOneVehicleType = function(callback) {
	let sqlString = `
		SELECT 
			vehicle_types.*,
			creators.name as created_by_name,
			updators.name as updated_by_name
		FROM 
			vehicle_types
		LEFT JOIN
			users as creators
			ON creators.id = vehicle_types.created_by
		LEFT JOIN
			users as updators
			ON updators.id = vehicle_types.updated_by
		WHERE 
			vehicle_types.id = ?
		`;
	db.query(sqlString, this.id, (error, result) => {
		if (error)
			throw error;

		let data = {};
		if (result.length)
			data = result[0];
		callback(null, data);
	});
}

Vehicle.prototype.createVehicle= function(callback) {
	sql_string = `INSERT INTO vehicle_types (
		name, 
		description,
		status,
		created_at,
		updated_at,
		created_by,
		updated_by
	) 
	VALUES(?, ?, ?, ?, ?, ?, ?)`;
	db.query(sql_string, [
		this.name, 
		this.description, 
		this.status, 
		this.created_at, 
		this.updated_at, 
		this.created_by, 
		this.updated_by
	], (error, result) => {
		if (error) 
			throw error;
		callback(null, result);
	});
}

Vehicle.prototype.updateVehicleType = function(callback) {
	sql_string = `UPDATE vehicle_types SET
		name = ?, 
		description = ?,
		status = ?,
		updated_at = ?,
		updated_by = ?
	WHERE id= ? `;
	db.query(sql_string, [
		this.name, 
		this.description, 
		this.status, 
		this.updated_at, 
		this.updated_by,
		this.id
	], (error, result) => {
		if (error) 
			throw error;
		callback(null, result);
	});
}

Vehicle.prototype.updateStatus = function(callback){
	var sqlString = `
		UPDATE 
			vehicle_types 
		SET 
			status = ?, 
			updated_by = ?, 
			updated_at =? 
		WHERE id = ?
	`;
	db.query(sqlString, [this.status, this.updated_by, this.updated_at, this.id], (error, result) => {
		if (error) 
			throw error;
		callback(null, result);
	});
};

Vehicle.prototype.deleteVehicleType = function(callback){
	let sqlString = "DELETE FROM vehicle_types WHERE id = ?";
	db.query(sqlString, this.id, (error, result) => {
		if (error) 
			throw error;
		callback(null, result);
	});
};
module.exports  = new Vehicle();