var db = require('../configs/db');
var CURRENT_TIMESTAMP = { toSqlString: function() { return 'CURRENT_TIMESTAMP()'; } };
const ParkingLot = function () {
	this.id = null;
	this.name = ""; 
	this.description = ""; 
	this.capacity = ""; 
	this.status = 0; 
	this.user_id = 0; 
	this.created_at = CURRENT_TIMESTAMP;
	this.updated_at = CURRENT_TIMESTAMP;
	this.created_by = 0;
	this.updated_by = 0;

};
 
ParkingLot.prototype.getAllParkingLots = function(conditions, callback) {

	let sqlString = `
		SELECT 
			parking_lots.*,
			creators.name as created_by_name,
			updators.name as updated_by_name
		FROM 
			parking_lots
		LEFT JOIN
			users as creators
			ON creators.id = parking_lots.created_by
		LEFT JOIN
			users as updators
			ON updators.id = parking_lots.updated_by
	`;

	if (conditions) {
		if (conditions.search) {
			sqlString += ` WHERE parking_lots.name LIKE '%${conditions.search}%'`;

		}
	}

	sqlString += ' ORDER BY parking_lots.created_at DESC';
	

	db.query(sqlString, (error, result) => {
		if (error)
			throw error;
		callback( result);
	});
}

ParkingLot.prototype.getOneParkingLot = function(callback) {
	let sqlString = `
		SELECT 
			parking_lots.*,
			creators.name as created_by_name,
			updators.name as updated_by_name
		FROM 
			parking_lots
		LEFT JOIN
			users as creators
			ON creators.id = parking_lots.created_by
		LEFT JOIN
			users as updators
			ON updators.id = parking_lots.updated_by
		WHERE 
			parking_lots.id = ?
		`;
	db.query(sqlString, this.id, (error, result) => {
		if (error)
			throw error;

		let data = {};
		if (result.length)
			data = result[0];
		callback(null, data);
	});
}

ParkingLot.prototype.createParkingLot= function(callback) {
	sql_string = `INSERT INTO parking_lots (
		name, 
		description,
		capacity,
		status,
		user_id,
		created_at,
		updated_at,
		created_by,
		updated_by
	) 
	VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)`;
	db.query(sql_string, [
		this.name, 
		this.description, 
		this.capacity,
		this.status, 
		this.user_id,
		this.created_at, 
		this.updated_at, 
		this.created_by, 
		this.updated_by
	], (error, result) => {
		if (error) 
			throw error;
		callback(result.insertId);
	});
}

ParkingLot.prototype.updateParkingLot = function(callback) {
	sql_string = `UPDATE ParkingLot_types SET
		name = ?, 
		description = ?,
		status = ?,
		updated_at = ?,
		updated_by = ?
	WHERE id= ? `;
	db.query(sql_string, [
		this.name, 
		this.description, 
		this.status, 
		this.updated_at, 
		this.updated_by,
		this.id
	], (error, result) => {
		if (error) 
			throw error;
		callback(null, result);
	});
}

ParkingLot.prototype.updateStatus = function(callback){
	var sqlString = `
		UPDATE 
			ParkingLot_types 
		SET 
			status = ?, 
			updated_by = ?, 
			updated_at =? 
		WHERE id = ?
	`;
	db.query(sqlString, [this.status, this.updated_by, this.updated_at, this.id], (error, result) => {
		if (error) 
			throw error;
		callback(null, result);
	});
};

ParkingLot.prototype.deleteParkingLot = function(callback){
	let sqlString = "DELETE FROM parking_lots WHERE id = ?";
	db.query(sqlString, this.id, (error, result) => {
		if (error) 
			throw error;
		callback(result);
	});
};
module.exports  = new ParkingLot();