var express = require('express');
var router = express.Router();
var bcrypt = require('bcrypt');
var user = require('../entities/user');
var parkingLot = require('../entities/parking-lot');

/* GET home page. */
router.get('/', function(req, res, next) {

	user.getOneUserByPermission(1, result => {
		let data = { data:result };
  		res.render('parking-controllers/index', data);
	});
});

router.get('/create', function(req, res, next) {
	parkingLot.getAllParkingLots({}, result => {

		res.render('parking-controllers/create', { lots: result });
	});
});


router.get('/edit/:id', function(req, res, next) {
  res.render('parking-controllers/edit', {  });
});

router.get('/show/:id', function(req, res, next) {
	const lotId = req.params.id;
	user.getOneLotController(lotId, result => {
		console.log(result);
		if (result)
			result = result[0];
		res.render('parking-controllers/show', { data: result });
	});	
  
});

router.post('/', function(req, res, next) {
	user.firstname = req.body.firstname;
	user.lastname = req.body.lastname;
	user.dob = req.body.dob;
	user.gender = req.body.gender;
	user.email = req.body.email;
	user.address = req.body.address;
	user.name = req.body.username;
	user.phone = req.body.phone;
	user.password = bcrypt.hashSync(req.body.password, 10);
	user.description = req.body.description;
	user.role_identifier = 1;
	user.created_by = 1;
	user.updated_by = 1;
	user.status = 1;

	user.parking_lot_id = req.body.parking_lot_id;

	user.createLotController(insert_id => {
		req.flash('success', 'You have created the resource successfully!');
	  	return res.redirect('/parking-controllers');
	});
});
module.exports = router;
