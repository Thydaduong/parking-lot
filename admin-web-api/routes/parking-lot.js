var express = require('express');
var router = express.Router();
var parkingLot = require('../entities/parking-lot');
const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

/* GET home page. */
router.get('/', function(req, res, next) {
	const conditions = {};
	parkingLot.getAllParkingLots(conditions, result => {

		res.render('parking-lots/index', { data: result});
	});
});

router.get('/create', function(req, res, next) {
  res.render('parking-lots/create', { title: 'Create Parking Lots' });
});

router.get('/show/:id', function(req, res, next) {
  res.render('parking-lots/show', { title: 'Show Parking Lots' });
});

router.get('/edit/:id', function(req, res, next) {
  res.render('parking-lots/edit', { title: 'Edit Parking Lots' });
});

router.post('/delete/:id', function(req, res, next) {
  res.redirect('/parking-lots');
});

router.post('/', function(req, res, next) {
	parkingLot.name = req.body.name; 
	parkingLot.description = req.body.description; 
	parkingLot.capacity = req.body.capacity; 
	parkingLot.status = 1; 
	parkingLot.user_id = 1; 

	parkingLot.created_by = 1;
	parkingLot.updated_by = 1;
	if (!req.body.resource_id) {
		parkingLot.createParkingLot(result => {
			req.flash('success', 'You have created the resource successfully!');
	  		res.redirect('/parking-lots');
		});
	}
	else {
		parkingLot.id = req.body.resource_id;
		parkingLot.deleteParkingLot(result => {
			req.flash('success', 'You have deleted the resource successfully!');
	  		res.redirect('/parking-lots');
		});
	}
	
});

router.post('/update', function(req, res, next) {
  res.redirect('/parking-lots');
});

module.exports = router;
