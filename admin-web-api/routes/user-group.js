var express = require('express');
var router = express.Router();
var userGroup = require('../entities/user-group');

var module_name = "User Group Management";
/* GET users listing. */

router.get('/', function(req, res, next) {
	var data = {
		title: module_name
	}
	userGroup.getAllUserGroups({}, (err, result) => {

  		res.render('user-groups/index', {data: result});
	});
});

router.post('/', function(req, res, next) {

	if (!req.body.name) {			
		console.log('Name is required')
		req.flash('error_name', 'Name is required')
		return res.redirect('back');
	}


	userGroup.name = req.body.name;
	userGroup.can_read = req.body.can_read || 1;
	userGroup.can_write = req.body.can_write || 0;
	userGroup.can_modify = req.body.can_modify || 0;
	userGroup.can_delete = req.body.can_delete || 0;
	userGroup.created_by = 1;
	userGroup.updated_by = 1;

	// update resource
	if (req.body.resource_id) {
		userGroup.id = req.body.resource_id;
		userGroup.updated_by = 1;
		userGroup.updateUserGroup(result => {
			req.flash('success', 'You have updated the resource successfully!');
			return res.redirect('/management/user-groups');
		});
	}
	else {
		// create new user group
		userGroup.createUserGroup(result => {
			req.flash('success', 'You have created the resource successfully!');
			return res.redirect('/management/user-groups');
		});
	}
	
});

router.get('/create', function(req, res, next) {
	var data = {
		title: module_name
	}
  res.render('user-groups/create', data);
});

router.get('/edit/:id', function(req, res, next) {
	userGroup.id = req.params.id || 0;
	userGroup.getOneUserGroup((error, result) => {
		res.render('user-groups/edit', { data: result });
	});
});

router.get('/show/:id', function(req, res, next) {
	userGroup.id = req.params.id || 0;
	userGroup.getOneUserGroup((error, result) => {
		res.render('user-groups/show', { data: result });
	});
});

router.post('/delete', function(req, res, next) {
	userGroup.id = req.body.resource_id;
	userGroup.deleteUserGroup(result => {
		req.flash('success', 'You have deleted the resource successfully!');
		return res.redirect('back');
	});
});

module.exports = router;
