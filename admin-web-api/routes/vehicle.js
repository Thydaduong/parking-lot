var express = require('express');
var router = express.Router();
var vehicle = require('../entities/vehicle');
var index = '/management/vehicles';
/* GET page. */
router.get('/', function(req, res, next) {
	const {search} = req.query;

	const queryConditions = {};
	if (search) 
		queryConditions.search = search;

	vehicle.getAllVehicleTypes(queryConditions, (error, result) => {
  		return res.render('vehicles/index', { data: result, old: queryConditions });
	});
});

router.get('/show/:id', function(req, res, next) {

	vehicle.id = req.params.id || 0;
	vehicle.getOneVehicleType((error, result) => {
	  	res.render('vehicles/show', { data: result });
	});
});


/* Create new resource*/
router.post('/', function(req, res, next) {
	const name = req.body.name;
	if (!name) {
		req.flash('error_name', 'Name is required');
		return res.redirect('back')
	}
	else {
		vehicle.name =  name;
		vehicle.description =  req.body.description;
		vehicle.status =  req.body.status || 1;
		vehicle.created_by =  1;
		vehicle.updated_by =  1;
		vehicle.createVehicle((error, result) => {
			req.flash('success', 'You have created the resource successfully!');
			return res.redirect(index);
		});
	}
});

router.get('/create', function(req, res, next) {
  res.render('vehicles/create', { title: 'Vehicle Type' });
});

router.get('/edit/:id', function(req, res, next) {
	const vehicle_id = req.params.id || 0;
	vehicle.id = vehicle_id;
	vehicle.getOneVehicleType((error, result) => {
	  	res.render('vehicles/edit', { data: result });
	});
});

router.post('/update', (req, res, next) => {
	vehicle.id = req.body.vehicle_id || 0;
	vehicle.name = req.body.name || "No name";
	vehicle.description = req.body.description || "";
	vehicle.status = req.body.status || 0;

	vehicle.updated_by = 1;

	vehicle.updateVehicleType((error, result) => {
		req.flash('success', 'You have created successfully!');
		return res.redirect(index);
	});
});

router.post('/status', (req, res, next) => {
	vehicle.id = req.body.vehicle_id || 0;
	vehicle.status = req.body.status || 0;
	vehicle.updated_by = 1;

	vehicle.updateStatus((error, result) => {
		return res.json({
			message: "updated"
		});
	});
});

router.post('/delete', (req, res, next) => {
	vehicle.id = req.body.resource_id || 0;
	vehicle.deleteVehicleType((error, result) => {
		req.flash('success', "You have deleted the resource successfully!");
		return res.redirect(index);
	});
});


module.exports = router;
