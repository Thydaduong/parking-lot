var express = require('express');
var router = express.Router();
var parkingLot = require('../entities/parking-lot');

/* GET home page. */
router.get('/', function(req, res, next) {
	parkingLot.getAllParkingLots({}, result => {
  		res.render('dashboard', { data: result, title: "Dashboard" });
	});
});

module.exports = router;
