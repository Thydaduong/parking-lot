var express = require('express');
var router = express.Router();
var module_name = "User Management";
/* GET users listing. */

router.get('/', function(req, res, next) {
	var data = {
		title: module_name
	}
  res.render('users/index', data);
});

router.get('/create', function(req, res, next) {
	var data = {
		title: module_name
	}
  res.render('users/create', data);
});

router.get('/edit', function(req, res, next) {
	var data = {
		title: module_name
	}
  res.render('users/edit', data);
});


module.exports = router;
