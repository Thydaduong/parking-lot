var express = require('express');
var router = express.Router();
var user = require('../entities/user');
var bcrypt = require('bcrypt');
// var hash = bcrypt.hashSync('password', 10);
router.post('/', function(req, res, next) {
	const username = req.body.name;
	const password = req.body.password;
	if (username && password) {
	user.getOneUserByName(username, get_user => {
			if (get_user) {
				//do something
				const match =  bcrypt.compareSync(password, get_user.password);
 
			    if(match) {
			    	req.session.user = get_user.id;
					return res.redirect('/');
			    }
			    else {
					req.flash('error', 'username or password is incorrect');
					return res.redirect("auths/login");
			    }
			}
			else {
				req.flash('error', 'username or password is incorrect');
				return res.redirect("auths/login");
			}

		});
	}
	else {
		req.flash('error_name', 'Username is required');
		req.flash('error_password', 'Password is required');
		return res.redirect("auths/login");
	}
	
  // res.render('auths/register', { title: 'Register' });
});

router.get('/login', function(req, res, next) {
  res.render('auths/login', { title: 'Login' });
});


router.get('/register', function(req, res, next) {
  res.render('auths/register', { title: 'Register' });
});

router.get('/logout', (req, res) => {
    req.session.destroy();
    res.redirect('/auths/login');
});

module.exports = router;
