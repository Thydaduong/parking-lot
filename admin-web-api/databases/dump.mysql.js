const connection = require('../configs/db');

const user_roles = (`
	INSERT INTO user_roles (name, identifier, number, created_at, update_at) VALUES
	('VERY STRONG', 'SUPER ADMIN', 4, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
	('STRONG', 'ADMIN', 3, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
	('MODERATE', 'USER', 2, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
	('WEAK', 'GUEST', 1, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
	('VERY WEAK', 'NONE', 0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP())
`);

connection.query(user_roles, (err, result) => {
	if (err) throw err;
	console.log('\x1b[32m', 'Dumped user_roles' ,'\x1b[0m');
	process.exit(0);
});

