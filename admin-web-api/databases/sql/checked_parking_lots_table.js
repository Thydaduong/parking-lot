module.exports = `CREATE TABLE IF NOT EXISTS checked_parking_lots (
	id INT NOT NULL AUTO_INCREMENT,
	plate_number_en VARCHAR (100)  NULL DEFAULT NULL,
	plate_number_kh VARCHAR (100)  NULL DEFAULT NULL,
	plate_image VARCHAR (255) NULL DEFAULT NULL,
	checked_in_at TIMESTAMP NULL DEFAULT NULL,
	checked_out_at TIMESTAMP NULL DEFAULT NULL,
	created_at TIMESTAMP NULL DEFAULT NULL,
	updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP(),
	PRIMARY KEY (id),
	vehicle_type_id INT NULL DEFAULT 0,
	parking_lot_controller_checked_in_by_id INT NULL DEFAULT 0,
	parking_lot_controller_checked_out_by_id INT NULL DEFAULT 0,
	parking_lot_id INT NULL DEFAULT 0,

	FOREIGN KEY (vehicle_type_id) REFERENCES vehicle_types(id),
	FOREIGN KEY (parking_lot_controller_checked_in_by_id) REFERENCES users(id),
	FOREIGN KEY (parking_lot_controller_checked_out_by_id) REFERENCES users(id),
	FOREIGN KEY (parking_lot_id) REFERENCES parking_lots(id)
)`;
