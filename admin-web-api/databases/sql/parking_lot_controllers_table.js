module.exports = `CREATE TABLE IF NOT EXISTS parking_lot_controllers (
	id INT NOT NULL AUTO_INCREMENT,
	created_at TIMESTAMP NULL DEFAULT NULL,
	updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP(),
	PRIMARY KEY (id),
	user_id INT NULL DEFAULT 0,
	parking_lot_id INT NULL DEFAULT 0,
	created_by INT NULL DEFAULT 0,
	updated_by INT NULL DEFAULT 0,
	FOREIGN KEY (user_id) REFERENCES users(id),
	FOREIGN KEY (parking_lot_id) REFERENCES parking_lots(id),
	FOREIGN KEY (created_by) REFERENCES users(id),
	FOREIGN KEY (updated_by) REFERENCES users(id)
)`;
