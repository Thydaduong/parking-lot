module.exports = (`
	CREATE TABLE IF NOT EXISTS  module_groups (
		id INT NOT NULL AUTO_INCREMENT,
		name VARCHAR (100) NOT NULL,
		created_at TIMESTAMP NULL DEFAULT NULL,
		updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP(),
		PRIMARY KEY (id),
		created_by INT NULL DEFAULT 0,
		FOREIGN KEY (created_by) REFERENCES users(id),
		updated_by INT NULL DEFAULT 0,
		FOREIGN KEY (updated_by) REFERENCES users(id)
	);
`);