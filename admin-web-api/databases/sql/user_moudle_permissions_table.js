module.exports = `CREATE TABLE IF NOT EXISTS user_moudle_permissions (
	id INT NOT NULL AUTO_INCREMENT,
	can_read BOOLEAN NULL DEFAULT 0,
	can_write BOOLEAN NULL DEFAULT 0,
	can_modify BOOLEAN NULL DEFAULT 0,
	can_delete BOOLEAN NULL DEFAULT 0,
	created_at TIMESTAMP NULL DEFAULT NULL,
	updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP(),
	PRIMARY KEY (id),
	created_by INT NULL DEFAULT 0,
	FOREIGN KEY (created_by) REFERENCES users(id),
	updated_by INT NULL DEFAULT 0,
	FOREIGN KEY (updated_by) REFERENCES users(id),
	user_id INT NULL DEFAULT 0,
	FOREIGN KEY (user_id) REFERENCES users(id),
	module_id INT NULL DEFAULT 0,
	FOREIGN KEY (module_id) REFERENCES module_groups(id)
)`;