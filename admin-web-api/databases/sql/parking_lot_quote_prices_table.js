module.exports = `CREATE TABLE IF NOT EXISTS parking_lot_quote_prices (
	id INT NOT NULL AUTO_INCREMENT,
	dollar DOUBLE (2, 2) NULL DEFAULT NULL,
	riel VARCHAR (100) NULL DEFAULT NULL,
	description TEXT NULL DEFAULT NULL,
	created_at TIMESTAMP NULL DEFAULT NULL,
	updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP(),
	PRIMARY KEY (id),
	vehicle_type_id INT NULL DEFAULT 0,
	parking_lot_id INT NULL DEFAULT 0,
	created_by INT NULL DEFAULT 0,
	updated_by INT NULL DEFAULT 0,
	FOREIGN KEY (vehicle_type_id) REFERENCES vehicle_types(id),
	FOREIGN KEY (parking_lot_id) REFERENCES parking_lots(id),
	FOREIGN KEY (created_by) REFERENCES users(id),
	FOREIGN KEY (updated_by) REFERENCES users(id)
)`;