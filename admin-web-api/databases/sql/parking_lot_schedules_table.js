module.exports = `CREATE TABLE IF NOT EXISTS parking_lot_schedules (
	id INT NOT NULL AUTO_INCREMENT,
	started_at TIME NULL DEFAULT NULL,
	closed_at TIME NULL DEFAULT NULL,
	created_at TIMESTAMP NULL DEFAULT NULL,
	updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP(),
	PRIMARY KEY (id),
	parking_lot_id INT NULL DEFAULT 0,
	day_id INT NULL DEFAULT 0,
	created_by INT NULL DEFAULT 0,
	updated_by INT NULL DEFAULT 0,
	FOREIGN KEY (parking_lot_id) REFERENCES parking_lots(id),
	FOREIGN KEY (day_id) REFERENCES days(id),
	FOREIGN KEY (created_by) REFERENCES users(id),
	FOREIGN KEY (updated_by) REFERENCES users(id)
)`;