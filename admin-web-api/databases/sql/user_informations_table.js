module.exports = `CREATE TABLE IF NOT EXISTS user_informations (
	id INT NOT NULL AUTO_INCREMENT,
	firstname VARCHAR (100) NOT NULL, 
	lastname VARCHAR (100) NOT NULL, 
	avatar VARCHAR (255) NULL, 
	gender VARCHAR (10) NULL DEFAULT 'MALE', 
	dob VARCHAR (255) NULL DEFAULT '00-00-1990', 
	address TEXT  NULL, 
	description TEXT  NULL, 
	created_at TIMESTAMP NULL DEFAULT NULL,
	updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP(),
	PRIMARY KEY (id),
	user_id INT NULL DEFAULT 0,
	created_by INT NULL DEFAULT 0,
	updated_by INT NULL DEFAULT 0,
	FOREIGN KEY (user_id) REFERENCES users(id),
	FOREIGN KEY (created_by) REFERENCES users(id),
	FOREIGN KEY (updated_by) REFERENCES users(id)
)`;