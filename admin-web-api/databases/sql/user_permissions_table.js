module.exports = `CREATE TABLE IF NOT EXISTS user_permissions (
	id INT NOT NULL AUTO_INCREMENT, 
	PRIMARY KEY (id),
	group_id INT NULL DEFAULT 0,
	user_id INT NULL DEFAULT 0,
	FOREIGN KEY (group_id) REFERENCES user_groups(id),
	FOREIGN KEY (user_id) REFERENCES users(id)
)`;