const connection = require('../configs/db');
var checked_parking_lots_table = require('./sql/checked_parking_lots_table');
var days_table = require('./sql/days_table');
var module_groups_table = require('./sql/module_groups_table');
var parking_lot_controllers_table = require('./sql/parking_lot_controllers_table');
var parking_lot_quote_prices_table = require('./sql/parking_lot_quote_prices_table');
var parking_lot_schedules_table = require('./sql/parking_lot_schedules_table');
var parking_lots_table = require('./sql/parking_lots_table');
var user_groups_table = require('./sql/user_groups_table');
var user_informations_table = require('./sql/user_informations_table');
var user_moudle_permissions_table = require('./sql/user_moudle_permissions_table');
var user_permissions_table = require('./sql/user_permissions_table');
var user_roles_table = require('./sql/user_roles_table');
var users_table = require('./sql/users_table');
var vehicle_types_table = require('./sql/vehicle_types_table');

// var dump = require('./dump.mysql');

const init = () => {
	try {
		console.info('migrating vehicle_types_table...');
		connection.query(vehicle_types_table, (error, result) => {
			if (error) 
				throw error;
				console.log('\x1b[32m', 'migrated vehicle_types_table' ,'\x1b[0m');
		});

		console.info('migrating user_roles_table...');
		connection.query(user_roles_table, (error, result) => {
			if (error) throw  error;
			console.log('\x1b[32m', 'migrated user_roles_table' ,'\x1b[0m');
		});

		console.info('migrating users_table...');
		connection.query(users_table, (error, result) => {
			if (error) 
				throw error;
			console.log('\x1b[32m', 'migrated users_table' ,'\x1b[0m');
		});

		console.info('migrating user_groups_table...');
		connection.query(user_groups_table, (error, result) => {
			if (error) throw error;
			console.log('\x1b[32m', 'migrated user_groups_table' ,'\x1b[0m');		
		});

		console.info('migrating user_permissions_table...');
		connection.query(user_permissions_table, (error, result) => {
			if (error) throw error;
			console.log('\x1b[32m', 'migrated user_permissions_table' ,'\x1b[0m');
		});

		console.info('migrating module_groups_table...');
		connection.query(module_groups_table, (error, result) => {
			if (error) throw error;
			console.log('\x1b[32m', 'migrated module_groups_table' ,'\x1b[0m');		
		});

		console.info('migrating user_moudle_permissions_table...');
		connection.query(user_moudle_permissions_table, (error, result) => {
			if (error) throw error;
			console.log('\x1b[32m', 'migrated user_moudle_permissions_table' ,'\x1b[0m');
		});

		console.info('migrating user_informations_table...');
		connection.query(user_informations_table, (error, result) => {
			if (error) throw error;
			console.log('\x1b[32m', 'migrated user_informations_table' ,'\x1b[0m');		
		});


		console.info('migrating parking_lots_table...');
		connection.query(parking_lots_table, (error, result) => {
			if (error) throw error;
			console.log('\x1b[32m', 'migrated parking_lots_table' ,'\x1b[0m');		
		});

		console.info('migrating days_table...');
		connection.query(days_table, (error, result) => {
			if (error) throw error;
			console.log('\x1b[32m', 'migrated days_table' ,'\x1b[0m');		
		});

		console.info('migrating parking_lot_schedules_table...');
		connection.query(parking_lot_schedules_table, (error, result) => {
			if (error) throw error;
			console.log('\x1b[32m', 'migrated parking_lot_schedules_table' ,'\x1b[0m');		
		});

		console.info('migrating parking_lot_quote_prices_table...');
		connection.query(parking_lot_quote_prices_table, (error, result) => {
			if (error) throw error;
			console.log('\x1b[32m', 'migrated parking_lot_quote_prices_table' ,'\x1b[0m');	
		});

		console.info('migrating parking_lot_controllers_table...');
		connection.query(parking_lot_controllers_table, (error, result) => {
			if (error) throw error;
			console.log('\x1b[32m', 'migrated parking_lot_controllers_table' ,'\x1b[0m');		
		});


		console.info('migrating checked_parking_lots_table...');
		connection.query(checked_parking_lots_table, (error, result) => {
			if (error) 
				throw error;
				console.log('\x1b[32m', 'migrated checked_parking_lots_table' ,'\x1b[0m');
				console.log('\x1b[32m', 'Proccess Successfully!!!' ,'\x1b[0m');
				process.exit(0);
		});

		// dump data
		// connection.query(dump.toString(), (error, result)=> {
		// 	if (error) throw error;
		// 	console.log('\x1b[32m', 'dumping data...' ,'\x1b[0m');
		// 	console.log('\x1b[32m', 'Proccess Successfully!!!' ,'\x1b[0m');
		// });

	} catch(e) {
		// statements
		console.log(e);
	}
	
}

module.exports = init();