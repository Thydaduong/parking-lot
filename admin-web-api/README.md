# Parking lot Managemnet Application
> This repo is a web application of parking management. 

## Organization
> None

## Documentation
> :books: URL : None

## Pre-requirements
- Node v8.11.2
- npm v6.1.0
- express v4.16.0
- mysql

## Deployment
> Development: None

> Production : Node

#### How to
1. Clone project from gitlab
```javscript
 	git clone https://gitlab.com/Thydaduong/parking-lot.git
```

2. Enter terminal directory
```javscript
 	cd parking-lot/admin-web-api
```

3. Install required js module
```javscript
 	npm install
```

4. Migrate database
```javscript
 	npm run migrate
```

5. run app
```javscript
 	npm run start
```
**** don't forget config your database credential at path/project/config/db.js
