requirejs.config({
	baseUrl: '.'
});

$(document).ready(function() {
	$(".custom-switch-input.switch").change(function(event) {
		var id  = $(this).attr('data-id');
		$.ajax({
			"url": "/management/vehicles/status",
			"type": "POST",
			"dataType": "json",
			"data": {
				"status": $(this)[0].checked? 1: 0,
				"vehicle_id": id
			},
		})
		.done(function(r) {
			console.log(r);
		})
		.fail(function(e) {
			console.log(e);
		});
	});


	$(".delete-action").click(function(event) {
		event.preventDefault();
		var form = $("#form-delete");
		var inputId = $("#resource-id");
		form.attr({
			"method": 'post',
			"action": $(this).attr('data-url')
		});
		inputId.val($(this).attr('data-id'));
	});

	$(".btn-clear").click(function(event) {
		var form = $(this).closest('form');
		form.find('input').val('');
		form.submit();
	});
});

function update(url, payload) {
	if (url && payload, id) {
		$.post(url, $.parseJSON(payload), function(data, textStatus, xhr) {
			console.log(data);
		});
	}
}
