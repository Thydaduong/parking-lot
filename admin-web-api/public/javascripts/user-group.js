
$(document).ready(function(){
	
	$("#permission-check-all").click(function() {
		var isChecked = $(this).prop('checked');
		$("#can-read").prop('checked', isChecked);
		$("#can-write").prop('checked', isChecked);
		$("#can-modify").prop('checked', isChecked);
		$("#can-delete").prop('checked', isChecked);
	});

	$("#can-write").click(function() {
		var isChecked = $(this).prop('checked');
		$("#permission-check-all").prop('checked', false); 
		$("#can-read").prop('checked', true); 
	});

	$("#can-modify").click(function() {
		var isChecked = $(this).prop('checked');
		$("#permission-check-all").prop('checked', false); 
		$("#can-read").prop('checked', true); 
	});

	$("#can-delete").click(function() {
		var isChecked = $(this).prop('checked');
		$("#permission-check-all").prop('checked', false); 
		$("#can-read").prop('checked', true); 
	});
});