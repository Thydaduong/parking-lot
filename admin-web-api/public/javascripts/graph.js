require(['c3', 'jquery'], function(c3, $) {
	$(document).ready(function(){
		var chart = c3.generate({
			bindto: '#chart-donut1', // id of chart wrapper
			data: {
				columns: [
				    // each columns data
					['data1', 0],
					['data2', 100]
				],
				type: 'donut', // default type of chart
				colors: {
					'data1': tabler.colors["green"],
					'data2': tabler.colors["green-light"]
				},
				names: {
				    // name of each serie
					'data1': 'Maximum',
					'data2': 'Minimum'
				}
			},
			axis: {
			},
			legend: {
	          show: false, //hide legend
			},
			padding: {
				bottom: 0,
				top: 0
			},
		});
	});


	$(document).ready(function(){
		var chart = c3.generate({
			bindto: '#chart-donut2', // id of chart wrapper
			data: {
				columns: [
				    // each columns data
					['data1', 0],
					['data2', 100]
				],
				type: 'donut', // default type of chart
				colors: {
					'data1': tabler.colors["green"],
					'data2': tabler.colors["green-light"]
				},
				names: {
				    // name of each serie
					'data1': 'Maximum',
					'data2': 'Minimum'
				}
			},
			axis: {
			},
			legend: {
	          show: false, //hide legend
			},
			padding: {
				bottom: 0,
				top: 0
			},
		});
	});
});