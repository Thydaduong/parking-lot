var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var flash = require('express-flash');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var userGroupRouter = require('./routes/user-group');
var parkingController = require('./routes/parking-controller');
var parkingLot = require('./routes/parking-lot');
var checkedLot = require('./routes/checked-lot');
var authRouter = require('./routes/auth');
var vehicleRouter = require('./routes/vehicle');

var app = express();
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser('keyboard cat'));
app.use(session({
    key: 'user_sid',
    secret: 'secret',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: 600000
    }
}));
app.use(flash());



// middleware function to check for logged-in users
var sessionChecker = (req, res, next) => {
	console.log(req.session.user);
    if (req.session.user) {
        next();
    } else {
        return res.redirect('/auths/login');
    }    
};


// route for Home-Page
// app.get('/', sessionChecker, (req, res) => {
//     res.redirect('/auths/login');
// });

app.use(express.static(path.join(__dirname, 'public')));

app.use('/auths', authRouter);
app.use('/', sessionChecker, indexRouter);
app.use('/management/users', sessionChecker, usersRouter);
app.use('/management/user-groups', sessionChecker, userGroupRouter);
app.use('/parking-controllers', sessionChecker, parkingController);
app.use('/parking-lots', sessionChecker, parkingLot);
app.use('/checked-lots', sessionChecker, checkedLot);
app.use('/management/vehicles', sessionChecker, vehicleRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
